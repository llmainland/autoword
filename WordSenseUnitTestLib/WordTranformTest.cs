﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using WordSense.Dictionary;
using WordSense.Scoring;
using System.Diagnostics;
using System.Threading;
using WordSense;

namespace WordSenseUnitTestLib
{
    [TestClass]
    public class WordTransformTest
    {
        public WordTransformTest()
        {
        }

        [TestInitialize]
        public void InitDictionary()
        {
            StaticDictionaries.InitializeSync();
        }

        //[TestMethod]
        //public void Test1() // test for GetWordType
        //{
        //    Assert.IsTrue((WordTransform.GetWordType("had").IsOfType(WordType.Verb)));
        //    Assert.IsTrue((WordTransform.GetWordType("have").IsOfType(WordType.Verb)));
        //    Assert.IsTrue(WordTransform.GetWordType("loving").IsOfType(WordType.Verb | WordType.Noun));
        //    Assert.IsTrue((WordTransform.GetWordType("loved").IsOfType(WordType.Verb)));
        //    Assert.IsTrue((WordTransform.GetWordType("loved").IsOfType(WordType.Adjective)));
        //    Assert.IsTrue((WordTransform.GetWordType("leaves").IsOfType(WordType.Verb | WordType.Noun)));
        //    Assert.IsTrue((WordTransform.GetWordType("glasses").IsOfType(WordType.Noun)));
        //    Assert.IsTrue((WordTransform.GetWordType("left").IsOfType(WordType.Verb | WordType.Adjective)));
        //}

        [TestMethod]
        public void Test2() // test for VerbGetSimpleForm
        {
            List<WordType> result = WordTransform.VerbGetSimpleForm("I had a dream");
            Assert.IsTrue(result[0].IsOfType(WordType.Pronoun));
            Assert.IsTrue(result[1].IsOfType(WordType.Verb));
            Assert.IsTrue(result[2].IsOfType(WordType.All));
            Assert.IsTrue(result[3].IsOfType(WordType.Noun));

            result = WordTransform.VerbGetSimpleForm("he has been working");
            Assert.IsTrue(result[0].IsOfType(WordType.Pronoun));
            Assert.IsTrue(result[1].IsOfType(WordType.Verb));
            Assert.IsTrue(result[2].IsOfType(WordType.Verb | WordType.Adjective));
            Assert.IsTrue(result[3].IsOfType(WordType.Verb));

            result = WordTransform.VerbGetSimpleForm("he thinks he is beautiful");
            Assert.IsTrue(result[0].IsOfType(WordType.Pronoun));
            Assert.IsTrue(result[1].IsOfType(WordType.Verb));
            Assert.IsTrue(result[2].IsOfType(WordType.Noun));
            Assert.IsTrue(result[3].IsOfType(WordType.Verb));
            Assert.IsTrue(result[4].IsOfType(WordType.Adjective));

            result = WordTransform.VerbGetSimpleForm("but he is not");
            Assert.IsTrue(result[0].IsOfType(WordType.Conjunction));
            Assert.IsTrue(result[1].IsOfType(WordType.Noun));
            Assert.IsTrue(result[2].IsOfType(WordType.Verb));
            Assert.IsTrue(result[3].IsOfType(WordType.Noun));

            result = WordTransform.VerbGetSimpleForm("it is finished");
            Assert.IsTrue(result[0].IsOfType(WordType.Noun));
            Assert.IsTrue(result[1].IsOfType(WordType.Verb));
            Assert.IsTrue(result[2].IsOfType(WordType.Verb | WordType.Adjective));
        }
    }
}
