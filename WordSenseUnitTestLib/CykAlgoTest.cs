﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using WordSense.Dictionary;
using WordSense.Scoring;
using System.Diagnostics;
using System.Threading;

namespace WordSenseUnitTestLib
{
    [TestClass]
    public class CykAlgoTest
    {
        public CykAlgoTest()
        {
        }

        [TestInitialize]
        public void InitDictionary()
        {
            StaticDictionaries.InitializeSync();
        }

        [TestMethod]
        public void Test1()
        {
            // test cyk algorithm
            //List<WordType> sequence = new List<WordType>();
            //WordType she1 = WordType.Pronoun;
            //WordType think = WordType.Verb;
            //WordType she2 = WordType.Pronoun;
            //WordType is1 = ~WordType.Unknown;
            //WordType beautiful = WordType.Adjective;
            //sequence.Add(she1);
            //sequence.Add(think);
            //sequence.Add(she2);
            //sequence.Add(is1);
            //sequence.Add(beautiful);
            //Assert.IsTrue(new CykAlgo().IsSentence(sequence));

            //// test cyk algorithm
            //List<WordType> sequence2 = new List<WordType>();
            //she1 = WordType.Pronoun;
            //she2 = WordType.Pronoun;
            //is1 = ~WordType.Unknown;
            //beautiful = WordType.Adjective;
            
            //sequence2.Add(she1);
            //sequence2.Add(she2);
            //sequence2.Add(is1);
            //sequence2.Add(beautiful);
            //Assert.IsFalse(new CykAlgo().IsSentence(sequence2));


            //List<WordType> sequence3 = new List<WordType>();
            //she1 = WordType.Pronoun;
            //think = WordType.Verb;
            //she2 = WordType.Pronoun;
            //is1 = ~WordType.Unknown;
            //WordType a = ~WordType.Unknown;
            //beautiful = WordType.Adjective;

            //sequence3.Add(she1);
            //sequence3.Add(think);
            //sequence3.Add(she2);
            //sequence3.Add(is1);
            //sequence3.Add(a);
            //sequence3.Add(beautiful);
            //Assert.IsTrue(new CykAlgo().IsSentence(sequence3));

            string word = "She is beautiful";
            bool isSentence = new CykAlgo().CheckIsSentenceWorks(word);
            Assert.IsTrue(isSentence);

            string word2 = "he likes football";
            bool isSentence2 = new CykAlgo().CheckIsSentenceWorks(word2);
            Assert.IsTrue(isSentence2);

            string word3 = "he likes to play football";
            CykAlgo cyk = new CykAlgo();
            //bool isSentence3 = cyk.CheckIsSentenceWorks(word3);
            double score = cyk.GetSentenceScore(word3);
            //Assert.IsFalse(isSentence3);
            //int times = cyk.GetTotalGrammars();
            double s = score;

            string word4 = "he she is good";
            bool isSentence4 = new CykAlgo().CheckIsSentenceWorks(word4);
            Assert.IsFalse(isSentence4);
            cyk = new CykAlgo();
            score = cyk.GetSentenceScore(word4);
            //times = cyk.GetTotalGrammars();
            s = score;

            string word5 = "she thinks she is beautiful";
            //Assert.IsFalse(new CykAlgo().CheckIsSentenceWorks(word5));
            score = (cyk = new CykAlgo()).GetSentenceScore(word5);
            //times = cyk.GetTotalGrammars();
            s = score;
            //score = new CykAlgo().GetSentenceScore();

            string word6 = "he turned to the east";
            bool isSentence6 = new CykAlgo().CheckIsSentenceWorks(word6);
            //Assert.IsTrue(isSentence6);
            score = (cyk = new CykAlgo()).GetSentenceScore(word6);
            //times = cyk.GetTotalGrammars();
            s = score;

            string word7 = "President Barack Obama blasted the Senate Friday for stalling on the nomination of ";
            bool isSentence7 = new CykAlgo().CheckIsSentenceWorks(word7);
            Assert.IsFalse(isSentence7);
            score = new CykAlgo().GetSentenceScore(word7);
        }
    }
}
