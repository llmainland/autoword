﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WordSense
{
    public sealed partial class PieMenuItem : UserControl
    {
        public PieMenuItem()
        {
            this.InitializeComponent();
        }

        public static readonly DependencyProperty SymbolProperty;
        public static readonly DependencyProperty HeaderProperty;

        static PieMenuItem()
        {
            PieMenuItem.SymbolProperty = DependencyProperty.Register("Symbol", typeof(string), typeof(PieMenuItem), new PropertyMetadata(string.Empty));
            PieMenuItem.HeaderProperty = DependencyProperty.Register("Header", typeof(string), typeof(PieMenuItem), new PropertyMetadata(string.Empty));
        }

        public string Symbol
        {
            get
            {
                return (string)base.GetValue(SymbolProperty);
            }
            set
            {
                base.SetValue(SymbolProperty, value);
            }
        }

        public string Header
        {
            get
            {
                return (string)base.GetValue(HeaderProperty);
            }
            set
            {
                base.SetValue(HeaderProperty, value);
            }
        }

        private void UserControl_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "MouseEnter", false);
        }

        private void UserControl_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "MouseExit", false);
        }
    }
}
