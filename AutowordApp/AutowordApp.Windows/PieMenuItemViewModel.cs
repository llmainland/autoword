﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordSense
{
    public class PieMenuItemViewModelCollection:List<PieMenuItemViewModel>
    {

    }

    public class PieMenuItemViewModel
    {
        public PieMenuItemViewModel(string symbol,string header, int rank)
        {
            //rank is relative.
            Symbol = symbol;
            Header = header;
            Rank = rank;
        }

        public PieMenuItemViewModel()
        {

        }

        public string Symbol { get; set; }
        public string Header { get; set; }
        public int Rank { get; set; }
        public IEnumerable<PieMenuItemViewModel> SecondaryItems { get; internal set; }
        public bool IsTerminalMenuItem { get { return SecondaryItems == null || SecondaryItems.Count() == 0; } }
        public PieMenuItemViewModel Parent { get; internal set; }
        public bool IsRootMenuItem { get { return Parent == null; } }

        public Action<Windows.UI.Text.ITextSelection> Execute;
        
    }
}
