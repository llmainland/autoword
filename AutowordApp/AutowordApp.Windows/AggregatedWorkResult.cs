﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutowordApp
{
    /// <summary>
    /// ugly patch to flatten the suggestion list.
    /// </summary>
    class IndividualWorkResult
    {
        public string Candidate { get; set; }
        public long StartingOption { get; set; }
        public static IEnumerable<IndividualWorkResult> CreateFromWorkResult(IEnumerable<WorkItem.WorkResult> results)
        {
            var resultList = new List<IndividualWorkResult>();
            if (results == null) return null;
            foreach (var result in results)
            {
                foreach (var candidate in result.Results)
                    resultList.Add(new IndividualWorkResult() { Candidate = candidate, StartingOption = result.EditingStartOffset });
            }
            return resultList;
        }
    }
}
