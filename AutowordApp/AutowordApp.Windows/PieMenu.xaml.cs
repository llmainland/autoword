﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WordSense
{
    public sealed partial class PieMenu : UserControl
    {
        public static DependencyProperty MenuItemsProperty;
        public static DependencyProperty IsMenuActiveProperty;

        static PieMenu()
        {
            MenuItemsProperty = DependencyProperty.Register("MenuItems", typeof(IEnumerable<PieMenuItemViewModel>), typeof(PieMenu), new PropertyMetadata(null));
            IsMenuActiveProperty = DependencyProperty.Register("IsMenuActive", typeof(bool), typeof(PieMenu), new PropertyMetadata(false));
        }

        public IEnumerable<PieMenuItemViewModel> MenuItems
        {
            get
            {
                return (IEnumerable<PieMenuItemViewModel>)base.GetValue(MenuItemsProperty);
            }
        }

        public bool IsMenuActive
        {
            get { return (bool)base.GetValue(IsMenuActiveProperty); }
            set
            {
                if (value)
                {
                    (this.Resources["show"] as Storyboard).Begin();
                }
                else
                {
                    (this.Resources["shrink"] as Storyboard).Begin();
                }
                base.SetValue(IsMenuActiveProperty, value);
            }
        }

        public PieMenu()
        {
            this.InitializeComponent();
            this.DataContextChanged += PieMenu_DataContextChanged;
            this.Loaded += PieMenu_Loaded;
        }

        void PieMenu_Loaded(object sender, RoutedEventArgs e)
        {
            (this.Resources["pushMenu"] as Storyboard).Completed += pushMenu_Completed;
            (this.Resources["popMenu"] as Storyboard).Completed += popMenu_Completed;
            (this.Resources["shrink"] as Storyboard).Completed += shrink_Completed;
        }

        private void shrink_Completed(object sender, object e)
        {
            MenuState = State.Ready;
        }

        private void popMenu_Completed(object sender, object e)
        {
            MenuState = State.Ready;
        }

        void PieMenu_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            if (args.NewValue == null || ( args.NewValue as IEnumerable<PieMenuItemViewModel> == null))
            {
                return;
            }
            //set menuitems.
            base.SetValue(MenuItemsProperty, args.NewValue);
            //must be rendered in context with the selected Texts.
            container.Children.Clear();
            foreach (var item in args.NewValue as IEnumerable<PieMenuItemViewModel>)
            {
                var currMenuItem = new PieMenuItem();
                currMenuItem.DataContext = item;
                container.Children.Add(currMenuItem);
            }
            Recalculate();//force a recalculate of arrays
        }

        public void Recalculate()
        {
            if (container.Children.Count == 0) return;
            var menuItems = container.Children;
            var radius = 90;// ActualHeight / 2;
            var perRankRadius = 2 * Math.PI / menuItems.Count;
            foreach (var item in menuItems)
            {
                var pieItem = (PieMenuItem)item;
                //just recalculate its translation and etc.
                var vm = pieItem.DataContext as PieMenuItemViewModel;
                var rank = vm.Rank;
                //rank is the rankth item (clock-wise).
                var translateY = -1.0 * radius;
                var transform = new CompositeTransform();
                var adjustedRadians = (rank - 1) * perRankRadius;
                transform.TranslateX = radius * Math.Sin(adjustedRadians);
                transform.TranslateY = radius * Math.Cos(adjustedRadians);
                pieItem.RenderTransform = transform;
            }
        }

        private void path_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            var button = sender as Button;
            var path = button.Content as Path;
            path.Fill = App.Current.Resources["AppBarBackgroundThemeBrush"] as SolidColorBrush;
            //VisualStateManager.GoToState(this, "MouseEnter", false);
        }

        private void path_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            var button = sender as Button;
            var path = button.Content as Path;
            path.Fill = App.Current.Resources["AccentBrush"] as SolidColorBrush;
       
            //VisualStateManager.GoToState(this, "MouseLeave", false);
        }

        Stack<IEnumerable<PieMenuItemViewModel>> menuLevels = new Stack<IEnumerable<PieMenuItemViewModel>>();

        PieMenuItemViewModel selectedMenuItem;

        public enum State
        {
            Ready,
            TransitioningToLowerContext,
            TransitioningToHigherContext
        }

        State state;
        public State MenuState
        {
            get
            {
                return state;
            }
            private set
            {
                if (value == PieMenu.State.Ready)
                {
                    this.IsHitTestVisible = true;
                }
                else
                {
                    this.IsHitTestVisible = false;
                }
                state = value;
            }
        }

        public Windows.UI.Text.ITextSelection CurrentActiveSelection { get; set; }

        private void path_PointerPressed(object sender, RoutedEventArgs e)
        {
            //if(MenuItems==null )
            Debug.Assert(MenuItems != null && MenuItems.Count() != 0); //don't even show if no menu is available.
            //should share the same parent.
            //Debug.Assert(menuLevels.Count != 0);
            MenuState = State.TransitioningToLowerContext;

            (this.Resources["pushMenu"] as Storyboard).Begin();

        }

        void pushMenu_Completed(object sender, object e)
        {
            //query state first
            if (MenuState == State.TransitioningToLowerContext)
            {
                if (menuLevels.Count != 0)
                {
                    var lowerLvl = menuLevels.Pop();
                    this.DataContext = lowerLvl;
                    (this.Resources["popMenu"] as Storyboard).Begin();
                }
                else
                {
                    //shrink to the minimal state.
                    (this.Resources["shrink"] as Storyboard).Begin();
                }
            }
            else if (MenuState == State.TransitioningToHigherContext)
            {
                menuLevels.Push(MenuItems);
                this.DataContext = selectedMenuItem.SecondaryItems;
                (this.Resources["popMenu"] as Storyboard).Begin();
            }
        }

        private void container_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var obj = e.OriginalSource;
            var control = obj as FrameworkElement;
            if (control == null || control.DataContext == null || control.DataContext.GetType() != typeof(PieMenuItemViewModel))
            {
                return;
            }
            var viewModel = selectedMenuItem = control.DataContext as PieMenuItemViewModel;
            if (viewModel.IsTerminalMenuItem)
            {
                if (viewModel.Execute != null && CurrentActiveSelection!=null)
                    viewModel.Execute.Invoke(CurrentActiveSelection);
            }
            else
            {
                //a request to go to next level is made.
                MenuState = State.TransitioningToHigherContext;
                (this.Resources["pushMenu"] as Storyboard).Begin();
            }
        }

        private void expander_Click(object sender, RoutedEventArgs e)
        {
            (this.Resources["popMenu"] as Storyboard).Begin();
            //e.Handled = true;
        }
    }
}
