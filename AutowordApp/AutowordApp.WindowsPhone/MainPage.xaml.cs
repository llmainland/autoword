﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Text;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using WordSense.Dictionary;
using WordSense.Scoring;
using WordSense.TextParsing;
using WordSense.WorkItem;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WordSense
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            var inputPane = InputPane.GetForCurrentView();
            inputPane.Showing += inputPane_Showing;
            inputPane.Hiding += inputPane_Hiding;
            this.Loaded += MainPage_Loaded;
        }
        double lastRecoverValue;

        void inputPane_Hiding(InputPane sender, InputPaneVisibilityEventArgs args)
        {
            this.InvalidateArrange();
            var inputPane = InputPane.GetForCurrentView();
            var height = inputPane.OccludedRect.Y;
            (this.Content as Grid).Height = lastRecoverValue;
            //content.Document.SetText(TextSetOptions.None, (this.Content as Grid).ActualHeight.ToString());
        }

        void inputPane_Showing(InputPane sender, InputPaneVisibilityEventArgs args)
        {
            this.InvalidateArrange();
            var inputPane = InputPane.GetForCurrentView();
            var height = inputPane.OccludedRect.Y;
            if (height != 0)
            {
                lastRecoverValue = (this.Content as Grid).ActualHeight;
                //content.Document.SetText(TextSetOptions.None, height.ToString());
                (this.Content as Grid).Height = height;
            }
        }
        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            content.Height = Windows.UI.Xaml.Window.Current.Bounds.Height;
            busy.Begin();

            //busy.RepeatBehavior = new RepeatBehavior(1);
            StaticDictionaries.InitializationReady += StaticDictionaries_InitializationReady;
            StaticDictionaries.InitializeAsync();
            //StaticDictionaries.InitDictionaries();
            //var content = new[] { "counting sheep on hill...", "comparing apples vs samsungs...", "readying up keyboards...", "taking care of a few things...", "checking your bank account...", "making harrasment calls to your contacts...", "finding your secret lover...", "learning how to do math...", "making friends with cortana...", "finalizing..." };
            //await Task.Run(async () =>
            //{
            //    for (int i = 0; i < content.Length; i++)
            //    {
            //        await Task.Delay(i * 100);
            //        await this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => textBlock.Text = content[i]);
            //  }
            //    await this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => done.Begin());
            //});
            //contextualMenu.DataContext = new PieMenuItemViewModelCollection();
            //this.Height = this.ActualHeight;
            var inputPane = InputPane.GetForCurrentView();
            inputPane.Showing += inputPane_Showing;
            inputPane.Hiding += inputPane_Hiding;
            //dispatcher.OnTaskCompletion += dispatcher_OnTaskCompletion;
            //suggestionList.ItemsSource = suggestionListItemSource;
            //var cvs = new CollectionViewSource();
            //cvs.Source = suggestionListItemSource;
            //cvs.SortDescription.Add();
            //ICollectionView view = cv
        }

        async void StaticDictionaries_InitializationReady(object sender, DictionaryInitializationReadyEventArgs e)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    busy.Stop();
                    busyGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    layoutRoot.Children.Remove(busyGrid);
                });
        }

        void insert(IndividualWorkResult result)
        {
            int insertIndex = suggestionListItemShadow.BinarySearch(result, priorityComparer);
            if (insertIndex < 0)
            {
                insertIndex = ~insertIndex;
            }
            suggestionList.Items.Insert(insertIndex, result);
            suggestionListItemShadow.Insert(insertIndex, result);
        }

        void remove(IndividualWorkResult result)
        {
            var flag1 = suggestionListItemShadow.Remove(result);
            var flag2 = suggestionList.Items.Remove(result);
            Debug.Assert(flag1 == flag2);
        }

        void removeInappropriateItems(string currDoc, int currEdit)
        {
            var removedItems = suggestionListItemShadow.Where(o => o.Candidate == null || o.StartingOption > currDoc.Length || (currEdit - (int)o.StartingOption + 1) <= 0 || !o.Candidate.StartsWith(currDoc.Substring((int)o.StartingOption, currEdit - (int)o.StartingOption + 1))).ToList();//force evaluation
            foreach (var item in removedItems)
            {
                suggestionListItemShadow.Remove(item);
                suggestionList.Items.Remove(item);
            }
        }

        //once per request id.
        //the stupid ObservableCollection on Windows Universal Platform does not provide View! No projection can be made.
        /// <summary>
        /// Replace this once ObservableCollection can have projections.  This is simply a shadow of listbox.items
        /// </summary>
        List<IndividualWorkResult> suggestionListItemShadow = new List<IndividualWorkResult>();
        IndividualWorkResultPriorityComparer priorityComparer = new IndividualWorkResultPriorityComparer();
        void dispatcher_OnTaskCompletion(object sender, WorkCompletedEventArgs e)
        {
            //var previousSelectedItem = suggestionList.SelectedItem; 
            //suggestionList.ItemsSource = suggestionListItemSource;
            if (e.Result.AssociatedWorkload.GetType() == typeof(BingBPoleWorkload))
            {
                remove(IndividualWorkResult.BingWorkResultPlaceholder);
                //suggestionListItemShadow.Remove(IndividualWorkResult.BingWorkResultPlaceholder);
            }
            if (e.Result.AssociatedWorkload.GetType() == typeof(SynonymsWorkload))
            {
                remove(IndividualWorkResult.SynoymousWorkResultPlaceholder);
                //suggestionListItemShadow.Remove(IndividualWorkResult.SynoymousWorkResultPlaceholder);
            }
            var results = IndividualWorkResult.CreateFromWorkResult(e.Result, PostResultScorer.Instance);
            int cnt = suggestionList.Items.Count;
            foreach (var result in results)
            {
                var addedResult = result;
                if (result.Candidate == null) continue;
                if (suggestionListItemShadow.Contains(result))
                {
                    var orig = suggestionListItemShadow.First<IndividualWorkResult>(o => o.Equals(result));
                    if (orig.Candidate.Count() < result.Candidate.Count()) addedResult = orig;
                    remove(orig);
                    //suggestionListItemSource.Remove(orig);
                    if (result.RequestId == orig.RequestId)
                        addedResult.SetChained();
                    addedResult.Priority = result.Priority + orig.Priority;
                }
                insert(addedResult);
            }
            //if (suggestionList.Items.Count != cnt)
            //{
            //    Reposition(suggestionList, true, Math.Min(suggestionList.MaxHeight, suggestionList.Items.Count * 40));
            //}//if (previousSelectedItem != null) suggestionList.SelectedItem = previousSelectedItem;
            suggestionList.SelectedIndex = -1;
        }
        int lastDocHash = -1;
        //UInt64 requestCnt = 0;
        //ObservableCollection<IndividualWorkResult> suggestionListItemSource = new ObservableCollection<IndividualWorkResult>();
        private void content_TextChanged(object sender, RoutedEventArgs e)
        {
            //Reposition();
            //var currId = requestCnt++;
            // var dispatcher = new WorkDispatcher();
            suggestionList.Visibility = Windows.UI.Xaml.Visibility.Visible;
            var textcontent = content.Text;
            //this.content.Document.GetText(Windows.UI.Text.TextGetOptions.None, out textcontent);
            if (lastDocHash == -1) lastDocHash = textcontent.GetHashCode();
            if (textcontent.GetHashCode() == lastDocHash) return;
            var dispatcher = new WorkDispatcher();
            dispatcher.OnObservableTaskCompletion += dispatcher_OnTaskCompletion;
            dispatcher.OnTaskFault += dispatcher_OnTaskFault;
            //suggestionListItemShadow.Clear();
            //suggestionList.Items.Clear();
            lastDocHash = textcontent.GetHashCode();
            var cursorPosition = content.SelectionStart + content.SelectionLength;
            //dbgInfo.Text = cursorPosition.ToString();
            if (cursorPosition == 0 || textcontent.Length == 0) return;
            var previous = cursorPosition - 1;
            removeInappropriateItems(textcontent, previous);
            var currentWord = TextParsing.TextProcessing.GetCurrentWord(textcontent, previous);
            int[] last10wordsIndices;
            var last10Words = TextParsing.TextProcessing.GetPrecedingWords(textcontent, previous, 10, out last10wordsIndices);
            var context = TextProcessing.GetLastSentence(textcontent, previous, true);
            var lastSentence = TextProcessing.GetLastSentence(textcontent, previous, false);
            var numBingSearch = 0;
            var numSynoymous = 0;

            if (textcontent[previous] == TextParsing.Constants.CarriageReturn)
            {
                //start of a new paragraph.
#if DEMO_MODE
                if (textcontent.Trim() != string.Empty)
                    dispatcher.QueueItems(new TextAnalyticsWorkload(8, -1, new[] { textcontent }));
#endif
            }
            //for (; !GetDelimiter().Contains(textcontent[previous]) && previous > 0; previous--) ;
            //var currentWord = textcontent.Substring(previous, cursorPosition - previous).Trim(GetDelimiter());
            //if (currentWord == string.Empty) return;
            //dbgInfo.Text += currentWord;
            //var taken = tokens.Take<string>(tokens.Length - 1);
            //var candidates = taken.AsParallel<string>().Where<string>(o => o.StartsWith(currentWord)).OrderByDescending<string, int>(o => taken.Count<string>(p => p == o)).Distinct().ToList();
            string formula;
            int formulaStart;
            //if calculator is active, disable other 
            if (TextProcessing.TryRetrieveFormula(textcontent, previous, out formula, out formulaStart))
            {
                dispatcher.QueueItems(new CalculatorWorkload(4, formulaStart, new[] { formula }));
            }
            else if (previous >= 2 && textcontent[previous] == TextParsing.Constants.Space && textcontent[previous - 1] == 's' && textcontent[previous - 2] == '\'')
            {
                dispatcher.QueueItems(new intelliSenseWorkload(11, previous, last10Words));
            }
            else if (!TextParsing.Constants.AllowedCharactersInFormula.Contains(textcontent[previous]))// || TextParsing.Constants.Space == textcontent[previous])
            {
                var reversedLast10Words = last10Words.Reverse().ToArray();
                for (int i = 0; i < last10Words.Length; i++)
                {
                    var queryParams = reversedLast10Words.Skip(i).ToArray();
                    if (queryParams.Length <= 4 && queryParams.Length > 1)
                    {
                        dispatcher.QueueItems(new QueryCompletionWorkload(1, last10wordsIndices[last10Words.Length - i - 1], queryParams, context));
                        //numGoogleSearch = 3; //maximum of 3 searches
                    }
                    if (queryParams.Length >= 3 && textcontent[previous] == TextParsing.Constants.Space && (cursorPosition + 1) == textcontent.Length)
                    {
                        dispatcher.QueueItems(new BingBPoleWorkload(2, last10wordsIndices[last10Words.Length - i - 1], queryParams));
                        numBingSearch = 1;
                    }
                    //                   if (i == 1 && currentWord.Trim().Length > 0)
                    //                       dispatcher.QueueItems(new BigramDoubleWorkload(8, last10wordsIndices[1], new[] { string.Format("{0} {1}", last10Words[1], currentWord.Trim()) }, context));
                }
                if (currentWord.Trim() != string.Empty)
                {
                    dispatcher.QueueItems(new TextSplitingWorkload(3, last10wordsIndices[0], new[] { textcontent, currentWord }, context));
                    //                                                new TrieWorkload(5, last10wordsIndices[0], new[] { currentWord }, context),
#if DEMO_MODE
                                                 new SynonymsWorkload(6, last10wordsIndices[0], new[] { currentWord }));
#endif
                    //                                                 new BigramWorkload(7, last10wordsIndices[0], new[] { currentWord }, context));
                    numSynoymous = 1;
                }
            }
            else if (TextParsing.Constants.EndOfSentence.Contains(textcontent[previous]))
            {
                //launch sentiment analysis
#if DEMO_MODE
                dispatcher.QueueItems(new SentimentAnalysisWorkload(10, -1, new[] { lastSentence }));
#endif
            }
            //calculator.


            //no need place holder when some earlier task already added placeholders
            if (numBingSearch != 0)
            {
                insert(IndividualWorkResult.BingWorkResultPlaceholder);
            }
            if (numSynoymous != 0)
            {
                insert(IndividualWorkResult.SynoymousWorkResultPlaceholder);
            }/*var httpClient = new HttpClient();
    var jsonComplete = await httpClient.GetAsync(new Uri("http://suggestqueries.google.com/complete/search?client=firefox&q=" + currentWord));
    var result = await jsonComplete.Content.ReadAsStringAsync();
    var array = Windows.Data.Json.JsonArray.Parse(result);
    */

            //if (currId == requestCnt - 1)
            //{
            //return;
            //suggestionList.ItemsSource = 
            dispatcher.RetrieveMergedResult();
            //IndividualWorkResult.CreateFromWorkResults(results);
            //}
            //dbgInfo.Text = string.Format("{0},{1}", requestCnt, currId);
        }

        void dispatcher_OnTaskFault(object sender, WorkExceptionEventArgs e)
        {
            if (e.ProblematicWorkload.GetType() == typeof(BingBPoleWorkload))
            {
                remove(IndividualWorkResult.BingWorkResultPlaceholder);
            }
            if (e.ProblematicWorkload.GetType() == typeof(SynonymsWorkload))
            {
                remove(IndividualWorkResult.SynoymousWorkResultPlaceholder);
            }
        }
        int keyPressCnt = 0;
        private void dbgInfo_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            dbgInfo.Text = keyPressCnt.ToString();
            if (e.Key != VirtualKey.Shift && e.Key != VirtualKey.Back)
                keyPressCnt++;
            if (suggestionList.Items.Count == 0) return;
            var item = suggestionList.Items.Cast<IndividualWorkResult>().FirstOrDefault<IndividualWorkResult>(o => o.IsPlaceHolder == false);
            if (suggestionList.SelectedItem == null || (suggestionList.SelectedItem as IndividualWorkResult).IsPlaceHolder) suggestionList.SelectedItem = item;
            if (suggestionList.SelectedItem == null) return;
            var escDown = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Escape).HasFlag(CoreVirtualKeyStates.Down);
            var shiftDown = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Shift).HasFlag(CoreVirtualKeyStates.Down);
            var tabDown = e.Key == VirtualKey.Tab;
            if (!tabDown)
            {
                var shift = e.Key == VirtualKey.Shift;
                if (!shift)
                {
                    if (e.Key == VirtualKey.Escape)
                    {
                        suggestionList.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    return;
                }
                if (suggestionList.Items.Count == 1 || suggestionList.SelectedIndex == 0) return;
                e.Handled = true;
                commitSelection(escDown);
                return;
            }
            //is switching selection?
            if (shiftDown)
            {
                suggestionList.SelectedIndex = (suggestionList.SelectedIndex + 1) % suggestionList.Items.Count;
                suggestionList.ScrollIntoView(suggestionList.SelectedItem);
                e.Handled = true;
                return;
            }
            e.Handled = true;
            //var currentText = "";
            //content.Document.GetText(Windows.UI.Text.TextGetOptions.None, out currentText);
            commitSelection(escDown);
        }
        private void commitSelection(bool isInsert, object overriden = null)
        {
            int insertPositionEnd = content.SelectionStart + content.SelectionLength;
            int insertPositionBegin = insertPositionEnd;
            //for (; insertPositionBegin > 0 && !GetDelimiter().Contains(currentText[insertPositionBegin - 1]); insertPositionBegin--) ;
            if (overriden != null) suggestionList.SelectedItem = (overriden as StackPanel).DataContext;
            insertPositionBegin = (int)(suggestionList.SelectedItem as IndividualWorkResult).StartingOption;
            //adjust for inserting before the last word.
            //var insertionPositionAdjusted = 
            //var before = currentText.Substring(0, insertPositionBegin == 0 ? 0 : insertPositionBegin);
            var replaced = suggestionList.SelectedItem == null ? String.Empty : (suggestionList.SelectedItem as IndividualWorkResult).Candidate;
            //var after = "";
            //if (insertPositionEnd != currentText.Length)
            //{
            //    after = currentText.Substring(insertPositionEnd, currentText.Length - insertPositionEnd);
            //}
            if (isInsert)
            {
                insertPositionBegin = insertPositionEnd = content.SelectionStart;
            }
            var pre = content.Text.Substring(0, insertPositionBegin);
            var end = content.Text.Substring(insertPositionEnd, content.Text.Length - insertPositionEnd);
            content.Text = string.Format("{0}{1}{2}", pre, replaced, end);
            content.SelectionStart = insertPositionBegin + replaced.Length;
            content.SelectionLength = 0;
            //content.Document.Selection.StartPosition = insertPositionBegin;
            //content.Document.Selection.EndPosition = insertPositionEnd;
            //content.Focus(Windows.UI.Xaml.FocusState.Programmatic);
        }

        private void content_SelectionChanged(object sender, RoutedEventArgs e)
        {
            //Reposition(suggestionList, true);
            if (content.SelectedText != string.Empty || content.SelectedText.Length == 0)
            {
                suggestionList.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }
            //contextualMenu.IsMenuActive = false;
            suggestionList.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }


        private void content_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Tab || e.Key == VirtualKey.Control || e.Key == VirtualKey.Shift) e.Handled = true;
        }

        private void suggestionList_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            //select the curren item.
            commitSelection(false, sender);
            e.Handled = true;
        }
    }
}
