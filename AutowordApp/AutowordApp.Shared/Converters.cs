﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace WordSense
{
    public class SuggestionListDataTemplate : DataTemplateSelector
    {
        //These are public properties that will be used in the Resources section of the XAML.
        public DataTemplate PlaceholdTemplate { get; set; }
        public DataTemplate NormalTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var currentFrame = Window.Current.Content as Frame;
            var currentPage = currentFrame.Content as Page;

            if (item != null)
            {
                var resultVM = item as IndividualWorkResult;
                if (!resultVM.IsPlaceHolder) return NormalTemplate;
                else return PlaceholdTemplate;
            }
            return base.SelectTemplateCore(item, container);
        }
    }

    public class ItemCountToVisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int count = (int)(double)value;
            return count == 0 ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class ItemToBorderThickness : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((ItemCollection)value).Count> 0 ? 0.5 : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
