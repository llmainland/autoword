﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace WordSense
{
    public class ThumbnailVM
    {
        public ThumbnailVM()
        {
            
        }
        public string SavePath { get; set; }
        public string Title { get; set; }
        public IRandomAccessStream ContentStream { get; set; }
        public SolidColorBrush Background { get; set; }


    }
}
