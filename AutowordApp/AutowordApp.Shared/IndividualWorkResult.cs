﻿using WordSense.WorkItem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace WordSense
{
    class IndividualWorkResultPriorityComparer : IComparer<IndividualWorkResult>
    {

        public int Compare(IndividualWorkResult x, IndividualWorkResult y)
        {
            return y.Priority - x.Priority > 0 ? 1 : -1;
        }
    }

    class IndividualWorkResult : INotifyPropertyChanged
    {
        public static IndividualWorkResult SynoymousWorkResultPlaceholder;
        public static IndividualWorkResult BingWorkResultPlaceholder;

        static Uri Google = new Uri("ms-appx:///assets/google-logo-colorful.jpg");
        static Uri bing = new Uri("ms-appx:///assets/bing-logo-colorful.png");
        static Uri localDict = new Uri("ms-appx:///assets/dict-logo-colorful.png");
        static Uri learnerDict = new Uri("ms-appx:///assets/learner-logo-colorful.png");
        static Uri calc = new Uri("ms-appx:///assets/calc-logo-colorful.png");
        static Uri syn = new Uri("ms-appx:///assets/synonyms.png");
        static Uri chained = new Uri("ms-appx:///assets/chained.png");
        static Uri sense = new Uri("ms-appx:///assets/sense.png");
        static Uri quote = new Uri("ms-appx:///assets/quote.png");
        static BitmapImage googleImg = new BitmapImage(Google);
        static BitmapImage bingImg = new BitmapImage(bing);
        static BitmapImage localDictImg = new BitmapImage(localDict);
        static BitmapImage learnerDictImg = new BitmapImage(learnerDict);
        static BitmapImage calcImg = new BitmapImage(calc);
        static BitmapImage synImg = new BitmapImage(syn);
        static BitmapImage chainedImg = new BitmapImage(chained);
        static BitmapImage senseImg = new BitmapImage(sense);
        static BitmapImage quoteImg = new BitmapImage(quote);

        static IndividualWorkResult()
        {
            //BitmapImage bi = new BitmapImage()
            //// BitmapImage.UriSource must be in a BeginInit/EndInit block.
            //bi.BeginInit();
            //bi.UriSource = new Uri(@"/sampleImages/cherries_larger.jpg", UriKind.RelativeOrAbsolute);
            //bi.EndInit();

            SynoymousWorkResultPlaceholder = new IndividualWorkResult();
            SynoymousWorkResultPlaceholder.SetUri(typeof(SynonymsWorkload));

            BingWorkResultPlaceholder = new IndividualWorkResult();
            BingWorkResultPlaceholder.SetUri(typeof(BingBPoleWorkload));
        }

        void firePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
       // public Workload AssociatedWork { get; private set; }
        public long StartingOption { get; set; }
        public string Candidate { get; set; }
        public string DisplayCandidate { get; set; }
        private BitmapImage img;
        public BitmapImage Image
        {
            get
            {
                return img;
            }
            set
            {
                if (value != img)
                {
                    img = value;
                    firePropertyChanged("ImageIdentifier");
                }
            }
        }
        private double priority;
        public double Priority
        {
            get
            {
                return priority;
            }
            set
            {
                if (value != priority)
                {
                    priority = value;
                    firePropertyChanged("Priority");
                }
            }
        }
        public long RequestId { get; private set; }
        public bool IsPlaceHolder { get; private set; }
        //static ImageSource googleS;
    
        static BitmapImage DetermineUri(Type workloadType)
        {
            var type = workloadType;
            if (type == typeof(BingBPoleWorkload))
            {
                return bingImg;
            }
            else if (type == typeof(QueryCompletionWorkload))
            {
                return googleImg;
            }
            else if (type == typeof(CalculatorWorkload))
            {
                return calcImg;
            }
            else if (type == typeof(TextSplitingWorkload))
            {
                return learnerDictImg;
            }
            else if (type == typeof(SynonymsWorkload))
            {
                return synImg;
            }
            else if (type == typeof(intelliSenseWorkload))
            {
                return senseImg;
            }
            else return localDictImg;
        }

        public static IEnumerable<IndividualWorkResult> CreateFromWorkResult(WorkResult result, Scoring.PostResultScorer scorer)
        {
            var img = DetermineUri(result.AssociatedWorkload.GetType());
            var lst = new List<IndividualWorkResult>();
            scorer.SetScore(result, Scoring.ScoringCriterion.All);
            for (int i = 0; i < result.Results.Count(); i++)
            {
                var item = result.Results.ElementAt(i);
                if (item != null)
                {
                    var customAttributes = result.AssociatedWorkload.GetType().GetTypeInfo().CustomAttributes;
                    var annotated = customAttributes.Any<CustomAttributeData>(data => data.AttributeType == typeof(WorkloadProducingLongResultsAttribute));
                    var displayCandidiate = annotated && item.Length > 20 ? string.Format("{0}...", item.Substring(0, 20)) : item;
                    img = annotated && item.StartsWith(" \"") ? quoteImg : img;
                    var iwResult = (new IndividualWorkResult() { DisplayCandidate = displayCandidiate, Candidate = item, StartingOption = result.EditingStartOffset, Image = img, IsPlaceHolder = false, priority = result.IndividualScores[i],RequestId = result.AssociatedWorkload.RequestId });
                    lst.Add(iwResult);
                }
            }
            return lst;
        }

        public void SetChained()
        {
            Image = chainedImg;
            IsChained = true;
        }

        public IndividualWorkResult()
        {
            IsPlaceHolder = true;
        }

        public void SetUri(Type t)
        {
            if (IsPlaceHolder == false) throw new InvalidOperationException("Only settable if this is a placeholder");
            Image = DetermineUri(t);
        }

        public override bool Equals(object obj)
        {
            var other = obj as IndividualWorkResult;
            if (other == null) return false;
            if (this.IsPlaceHolder && other.IsPlaceHolder) return base.Equals(obj);
            if (this.Candidate == null || other.Candidate == null) return false;
            if (other.Candidate == Candidate) return true;
            if (other.StartingOption == this.StartingOption) return other.Candidate == Candidate;
            if (other.StartingOption + other.Candidate.Length != this.StartingOption + this.Candidate.Length) return false;
            IndividualWorkResult longResult, shortResult;            
            if (other.StartingOption < this.StartingOption)
            {
                longResult = other;
                shortResult = this;
            }
            else
            {
                longResult = this;
                shortResult = other;
            }
            int offset = (int)(shortResult.StartingOption - longResult.StartingOption);            
            return longResult.Candidate.Substring(offset, Math.Min(shortResult.Candidate.Length, longResult.Candidate.Length - offset)) == shortResult.Candidate;
        }

        public override int GetHashCode()
        {
            return Candidate.GetHashCode();
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsChained { get; set; }
    }
}
