﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using WordSense.Trie;
using System.Linq;
using WordSense.Scoring;

namespace WordSense
{
   public static class Utils
    {
        public static void AddMany<T>(this ObservableCollection<T> obj, IEnumerable<T> items)
        {
            foreach (var item in items) obj.Add(item);
        }

        public static IEnumerable<string> GetAllByPrefix<TValue>(this IEnumerable<Trie<TValue>> dictionaries, string prefix)
        {
            var lst = new List<string>();
            foreach (var dict in dictionaries)
            {
                lst.AddRange(dict.GetByPrefix(prefix).Select<TrieEntry<TValue>, string>(o => o.Key));//))
            }
            return lst;
        }

        public static void Foreach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable) action(item);
        }

        public static int GetWordCount(this string str)
        {
            int cnt = 1;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ' ') cnt++;
            }
            return cnt;
        }

        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dict, IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            items.Foreach<KeyValuePair<TKey, TValue>>(o => dict.Add(o.Key, o.Value));
        }

        public static bool IsOfType(this WordType type, WordType targetType)
        {
            return (type & targetType) != WordType.Unknown;
        }
    }
}