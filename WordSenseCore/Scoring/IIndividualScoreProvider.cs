﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordSense.Scoring;

namespace WordSense.Scoring
{
    public interface IIndividualScoreProvider
    {
        double GetIndividualScore(string response, string context);
    }
}
