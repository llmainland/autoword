﻿using System;
using System.Collections.Generic;
using System.Text;
using WordSense.Dictionary;


namespace WordSense.Scoring
{
    public class WordTransform
    {

        public static List<WordType> VerbGetSimpleForm(string word)
        {
            string[] words = word.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            List<WordType> sequenceOfWordType = new List<WordType>();
            foreach (string singleWord in words)
            {
                sequenceOfWordType.Add(GetWordType(singleWord));
            }
            return sequenceOfWordType;
        }

        public static WordType GetWordType(string word)
        {
            word = word.ToLowerInvariant();
            WordType tempWordType = WordType.All;
            WordType wordType = WordType.Unknown;
            String originalForm = "";
            // simple form in dictionary
            if (StaticDictionaries.WordTypeDictionary.TryGetValue(word, out tempWordType)) wordType = tempWordType;
            // found in special transformation dicionary
            if (StaticDictionaries.WordTransformationRecoveryDictionary.TryGetValue(word, out originalForm))
            {
                if (StaticDictionaries.WordTypeDictionary.TryGetValue(originalForm, out tempWordType) && ((tempWordType & WordType.Verb) != WordType.Unknown)) tempWordType = tempWordType | WordType.Verb;
                wordType = wordType | tempWordType;
            }
            if (wordType != WordType.Unknown) return wordType;
            // verb -ing form, return its tempWordType in dictionary and noun
            if (RegexTemplate.rgxI3.IsMatch(word))
            {
                if (RegexTemplate.rgxI1.IsMatch(word))
                {
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 4) + "ie", out tempWordType)) wordType = tempWordType | WordType.Noun;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3), out tempWordType)) wordType = wordType | tempWordType | WordType.Noun;
                    if (wordType != WordType.Unknown) return wordType;
                    return WordType.All;
                }
                if (RegexTemplate.rgxI2.IsMatch(word))
                {
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 4), out tempWordType)) wordType = tempWordType | WordType.Noun;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3), out tempWordType)) wordType = wordType | tempWordType | WordType.Noun;
                    if (wordType != WordType.Unknown) return wordType;
                    return WordType.All;
                }
                if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3), out tempWordType)) return tempWordType | WordType.Noun;
                return WordType.All;
            }
            // verb -s form or noun pural form, return its tempWordType in dictionary
            if (RegexTemplate.rgxS1.IsMatch(word))
            {
                if (RegexTemplate.rgxS2.IsMatch(word))
                {
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 2), out tempWordType)) wordType = tempWordType;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 1), out tempWordType)) wordType = wordType | tempWordType;
                    if (wordType != WordType.Unknown) return wordType;
                    return WordType.All;
                }
                if (RegexTemplate.rgxS3.IsMatch(word))
                {
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3) + "y", out tempWordType)) wordType =  tempWordType;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 1), out tempWordType)) wordType = wordType | tempWordType;
                    if (wordType != WordType.Unknown) return wordType;
                    return WordType.All;
                }
                if (RegexTemplate.rgxS4.IsMatch(word))
                {
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3) + "f", out tempWordType)) wordType = tempWordType;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3) + "fe", out tempWordType)) wordType = wordType |tempWordType;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 1), out tempWordType)) wordType = wordType |tempWordType;
                    if (wordType != WordType.Unknown) return wordType;
                    return WordType.All;
                }
                if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 1), out tempWordType)) return tempWordType;
                return WordType.All;
            }
            // verb -ed form, return its tempWordType in dictionary and adj.
            if (RegexTemplate.rgxP3.IsMatch(word))
            {
                if (RegexTemplate.rgxP1.IsMatch(word))
                {
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3) + "y", out tempWordType)) wordType = tempWordType | WordType.Adjective;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 2), out tempWordType)) wordType = wordType |tempWordType | WordType.Adjective;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 1), out tempWordType)) wordType = wordType |tempWordType | WordType.Adjective;
                    if (wordType != WordType.Unknown) return wordType;
                    return WordType.All;
                }
                if (RegexTemplate.rgxP2.IsMatch(word))
                {
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 3), out tempWordType)) wordType = tempWordType | WordType.Adjective;
                    if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 2), out tempWordType)) wordType = wordType | tempWordType | WordType.Adjective;
                    if (wordType != WordType.Unknown) return wordType;
                    return WordType.All;
                }
                if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 2), out tempWordType)) wordType = tempWordType | WordType.Adjective;
                if (StaticDictionaries.WordTypeDictionary.TryGetValue(word.Substring(0, word.Length - 1), out tempWordType)) wordType = wordType | tempWordType | WordType.Adjective;
                if (wordType != WordType.Unknown) return wordType;
                return WordType.All;
            }
            return WordType.All;
        }
    }
}
