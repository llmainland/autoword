﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace WordSense.Scoring
{
    class BnfGrammar
    {
        // this class saves a list of grammar definitions
        const string NP = "NP";
        const string VP = "VP";
        const string PP = "PP";
        const string S = "S";
        const string U = "U";

        const string VERB = "Verb";
        const string NOUN = "Noun";
        const string PRO = "Pronoun";

        const string PREP = "PREP";
        const string ADV = "ADV";
        const string ADJ = "ADJ";
        const string CONJ = "CONJ";

        const string SPACE = " ";

        // hashtable saves non-terminals and their definitions
        Dictionary<string, WordType[]> defsOfTerminals = new Dictionary<string, WordType[]>();
        Dictionary<string, string[]> defsOfNonterminals = new Dictionary<string, string[]>();
        Dictionary<string, int> nonterminalIndices = new Dictionary<string, int>();


        string startSymbol = S;
        //public int DictSize = 0;

        public BnfGrammar()
        {
            // add def of NP
            string[] defOfNp_nonterminal = new string[1];
            defOfNp_nonterminal[0] = NP + SPACE + PP;
            defsOfNonterminals[NP] = defOfNp_nonterminal;

            WordType[] defOfNp_terminal = new WordType[2];
            defOfNp_terminal[0] = WordType.Pronoun;
            defOfNp_terminal[1] = WordType.Noun;
            defsOfTerminals[NP] = defOfNp_terminal;
            nonterminalIndices[NP] = 0;
            //string[] defOfNp = new string[3];
            //defOfNp[0] = PRO;
            //defOfNp[1] = NOUN;
            //defOfNp[2] = NP + SPACE + PP;
            //defs[NP] = defOfNp;
            //nonterminalIndices[NP] = 0;

            // add def of VP
            //string[] defOfVp = new string[5];
            //defOfVp[0] = VERB;
            //defOfVp[1] = VP + SPACE + NP;
            //defOfVp[2] = VP + SPACE + ADJ;
            //defOfVp[3] = VP + SPACE + PP;
            //defOfVp[4] = VP + SPACE + ADV;
            //defs[VP] = defOfVp;
            string[] defOfVp_nonterminal = new string[4];
            defOfVp_nonterminal[0] = VP + SPACE + NP;
            defOfVp_nonterminal[1] = VP + SPACE + ADJ;
            defOfVp_nonterminal[2] = VP + SPACE + PP;
            defOfVp_nonterminal[3] = VP + SPACE + ADV;
            defsOfNonterminals[VP] = defOfVp_nonterminal;

            WordType[] defOfVp_terminal = new WordType[1];
            defOfVp_terminal[0] = WordType.Verb;
            defsOfTerminals[VP] = defOfVp_terminal;
            nonterminalIndices[VP] = 1;

            // add def of PP
            string[] defOfPp = new string[1];
            defOfPp[0] = PREP + SPACE + NP;
            defsOfNonterminals[PP] = defOfPp;
            nonterminalIndices[PP] = 2;

            // add def of S
            string[] defOfS = new string[2];
            defOfS[0] = NP + SPACE + VP;
            defOfS[1] = S + SPACE + U;
            defsOfNonterminals[S] = defOfS;
            nonterminalIndices[S] = 3;

            // add def of U
            string[] defOfU = new string[1];
            defOfU[0] = CONJ + SPACE + S;
            defsOfNonterminals[U] = defOfU;
            nonterminalIndices[U] = 4;

            // add def of Prep
            WordType[] defOfPrep = new WordType[1];
            defOfPrep[0] = WordType.Preposition;
            defsOfTerminals[PREP] = defOfPrep;
            nonterminalIndices[PREP] = 5;

            //string[] defOfPrep = new string[1];
            //defOfPrep[0] = WordType.Preposition.ToString();
            //defs[PREP] = defOfPrep;
            //nonterminalIndices[PREP] = 5;

            // add def of Adv
            WordType[] defOfAdv = new WordType[1];
            defOfAdv[0] = WordType.Adverb;
            //defOfAdv[ADV] = defOfAdv;
            defsOfTerminals[ADV] = defOfAdv;
            nonterminalIndices[ADV] = 6;

            // add def of Adj
            WordType[] defOfAdj = new WordType[1];
            defOfAdj[0] = WordType.Adjective;
            defsOfTerminals[ADJ] = defOfAdj;
            nonterminalIndices[ADJ] = 7;
            //string[] defOfAdj = new string[1];
            //defOfAdj[0] = WordType.Adjective.ToString();
            //defs[ADJ] = defOfAdj;
            //nonterminalIndices[ADJ] = 7;

            // add def of Conj
            WordType[] defOfConj = new WordType[1];
            defOfConj[0] = WordType.Conjunction;
            defsOfTerminals[CONJ] = defOfConj;
            nonterminalIndices[CONJ] = 8;

            //DictSize = defs.Count;
        }

        public int GetNonterminalDictSize()
        {
            return nonterminalIndices.Count;
        }

        public int GetTerminalDictSize()
        {
            return defsOfTerminals.Count;
        }

        public Dictionary<string, WordType[]> GetTerminalDict()
        {
            return defsOfTerminals;
        }

        public Dictionary<string, string[]> GetNonterminalDict()
        {
            return defsOfNonterminals;
        }

        //public Dictionary<string, string[]> GetDefs()
        //{
        //    return defs;
        //}

        public Dictionary<string, int> GetNonterminalsIndices()
        {
            return nonterminalIndices;
        }

        public string GetStartSymbol()
        {
            return startSymbol;
        }
    }
}
