﻿using System;
using System.Collections.Generic;
using System.Text;
using WordSense.Dictionary;
using System.Text.RegularExpressions;

namespace WordSense.Scoring
{
    // implement CYK algorithm
    public class CykAlgo
    {
        //int timesMatchGrammar = 0;
        double score = 0d;
        BnfGrammar bnf = new BnfGrammar();

        public int[] IsSentence(List<WordType> sequence)
        {
            Dictionary<string, int> nonterminalsIndices = bnf.GetNonterminalsIndices();
            int wordsNum = sequence.Count, timesMatchGrammar = 0;
            int nonterminalsNum = bnf.GetNonterminalDictSize();

            bool[, ,] cube = new bool[wordsNum, wordsNum, nonterminalsNum];
            for (int i = 0; i < wordsNum; i++)
                for (int j = 0; j < wordsNum; j++)
                    for (int k = 0; k < nonterminalsNum; k++)
                        cube[i, j, k] = false;

            // set terminals to be true
            for (int i = 0; i < wordsNum; i++)
                foreach (var pair in bnf.GetTerminalDict())
                {
                    var length = pair.Value.Length;
                    for (int j = 0; j < pair.Value.Length; j++)
                        if (sequence[i].IsOfType(pair.Value[j]))
                            cube[0, i, nonterminalsIndices[pair.Key]] = true;
                }

            for (int i = 2; i <= wordsNum; i++)
                for (int j = 1; j <= wordsNum - i + 1; j++)
                    for (int k = 1; k <= i - 1; k++)
                        foreach (var pair in bnf.GetNonterminalDict())
                            for (int index = 0; index < pair.Value.Length; index++)
                            {
                                string[] list = pair.Value[index].Split(' ');
                                if (list.Length == 2 && cube[k - 1, j - 1, nonterminalsIndices[list[0]]]
                                    && cube[i - k - 1, j + k - 1, nonterminalsIndices[list[1]]])
                                {
                                    cube[i - 1, j - 1, nonterminalsIndices[pair.Key]] = true;
                                    timesMatchGrammar++;
                                }
                            }


            // return true if it is in language, otherwise false
            string startSymbol = bnf.GetStartSymbol();
            //if (cube[wordsNum - 1, 0, nonterminalsIndices[startSymbol]])
            //{
                
            //    //score = 1d;
            //    //return true;
            //}
            //return false;
            return new int[]{wordsNum, timesMatchGrammar};
        }

        public double GetSentenceScore(List<WordType> sequence, int num)
        {
            //if (IsSentence(sequence)) return 1d;
            //if (((double)timesMatchGrammar) / IsSentence(sequence) > 1d) return 1d;
            //return ((double)timesMatchGrammar) / num;
            int[] result = IsSentence(sequence);
            int wordsNum = result[0], timesMatchGrammar = result[1];
            if ((((double)timesMatchGrammar) / wordsNum) > 1d) return 1d;
            return ((double)timesMatchGrammar) / wordsNum;
        }

        public double GetSentenceScore(string word)
        {
            return GetSentenceScore(WordTransform.VerbGetSimpleForm(word), 0);
        }

        //public Boolean CheckIsSentenceWorks(string word)
        //{
        //    return IsSentence(WordTransform.VerbGetSimpleForm(word));
        //}
    }
}
