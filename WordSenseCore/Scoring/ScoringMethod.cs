﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordSense.Scoring
{
    class ScoringMethod
    {
        public static double RangeScore(double score, double standard, double max, double min, double lowerBound)
        {
            return Math.Max(lowerBound, Math.Exp(-1 * Math.Pow(score - standard, 2) / ( Math.Pow(0.5*(max - min), 2))));
        }
    }
}
