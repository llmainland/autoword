﻿using WordSense.Trie;
using WordSense.WorkItem;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using WordSense.Dictionary;
namespace WordSense.Scoring
{
    [Flags]
    public enum ScoringCriterion : uint
    {
        None = 0,
        Coherence = 1,
        Grammar = 2,
        Personality = 4,
        FrequencyAndLength = 8,
        All = UInt32.MaxValue
    }

    public class PostResultScorer
    {
        public static PostResultScorer Instance { get; private set; }
        public Dictionary<ScoringCriterion, double> CriterionWeight { get; set; }
        public Dictionary<ScoringCriterion, double> PerfectValues { get; set; }
        //public Dictionary<ScoringCriterion, IIndividualScoreProvider> ScorerDictionary { get; set; }
        static PostResultScorer()
        {
            Instance = new PostResultScorer();
        }
        private PostResultScorer()
        {
            CriterionWeight = new Dictionary<ScoringCriterion, double>();
            CriterionWeight[ScoringCriterion.Coherence] = 0.5;
            PerfectValues = new Dictionary<ScoringCriterion, double>();
            PerfectValues[ScoringCriterion.Coherence] = 0.5;
            //ScorerDictionary = new Dictionary<ScoringCriterion, IIndividualScoreProvider>();
        }
        public void SetScore(WorkResult result, ScoringCriterion criterion)
        {
            //return;
            //score emotional.
            for (int i = 0; i < result.Results.Count(); i++)
            {
                var currResult = result.Results.ElementAt(i);//fast because it is just a list, as we know it.
                if (result.AssociatedWorkload.GetType() != typeof(BingBPoleWorkload))
                    //result.IndividualScores[i] = 1;
                    result.IndividualScores[i] = GetIndividualScore(currResult, criterion, result.AssociatedWorkload.Context, result.IndividualScores[i]);
            }
        }

        public double GetIndividualScore(string response, ScoringCriterion criterion, string context, double initial = 1)
        {
            List<IIndividualScoreProvider> selectedScorers = new List<IIndividualScoreProvider>();
            var segments = response.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if ((criterion & ScoringCriterion.Coherence) != ScoringCriterion.None)
            {
                selectedScorers.Add(new CoherenceScorer(PerfectValues[ScoringCriterion.Coherence]));
            }
            if ((criterion & ScoringCriterion.Grammar) != ScoringCriterion.None)
            {
                //selectedScorers.Add(new GrammarScorer());
            }
            if ((criterion & ScoringCriterion.FrequencyAndLength) != ScoringCriterion.None)
            {
                selectedScorers.Add(new FrequencyAndLengthScorer());
            }
            initial *= selectedScorers.AsParallel<IIndividualScoreProvider>().Select<IIndividualScoreProvider, double>(o => o.GetIndividualScore(response, context)).Aggregate<double, double>(1.0, (prod, next) => prod * next);
            return initial;
            //double totalScore = 1;
            //if ((criterion & ScoringCriterion.Coherence) != ScoringCriterion.None)
            //{
            //    var score = response.Split(' ').Sum<string>(str =>
            //        {
            //            if (StaticDictionaries.EmotionalDictionary.ContainsKey(str))
            //                return StaticDictionaries.EmotionalDictionary[str];
            //            return 0;
            //        });
            //    score = score > 1 ? 1 : score;
            //    score = score < -1 ? -1 : score;
            //    totalScore -= Math.Abs(PerfectValues[ScoringCriterion.Coherence] - score);
            //    return totalScore = totalScore > 1 ? 1 : totalScore < -1 ? -1 : totalScore;
            //}
            //return 0.5;
        }
    }
}
