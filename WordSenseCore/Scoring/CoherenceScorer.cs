﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordSense.Dictionary;
using WordSense.Scoring;

namespace WordSense.Scoring
{
    class CoherenceScorer : IIndividualScoreProvider
    {
        double currValue;
        public CoherenceScorer(double perfectValue)
        {
            currValue = perfectValue;
        }
        public double GetIndividualScore(string response, string context)
        {
            double perfectValue = currValue;
            var segments = response.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            double score = segments.Sum<string>(str =>
            {
                if (StaticDictionaries.EmotionalDictionary.ContainsKey(str))
                    return StaticDictionaries.EmotionalDictionary[str];
                return 0;
            });
            score = ScoringMethod.RangeScore(score, perfectValue, 1, -1, 0.1);
            return score;
        }
    }
}
