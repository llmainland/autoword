﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordSense.Scoring;

namespace WordSense.Scoring
{
    class GrammarScorer : IIndividualScoreProvider
    {
        static CykAlgo grammarScorer = new CykAlgo();
        public double GetIndividualScore(string response, string context)
        {            
            return grammarScorer.GetSentenceScore(string.Format("{0} {1}", context, response));
        }
    }
}
