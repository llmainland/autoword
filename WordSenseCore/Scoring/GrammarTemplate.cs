﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace WordSense.Scoring
{
    [Flags]
    public enum WordType : uint
    {
        Unknown = 0,
        Noun = 1,
        Verb = 2,
        Adverb = 4,
        Preposition = 8,
        Adjective = 16,
        Pronoun = 32,
        Interjection = 64,
        Conjunction = 128,
        All = UInt32.MaxValue
    }
    public static class RegexTemplate {
        public static Regex rgxI1 = new Regex(@"^.*ying$");
        public static Regex rgxI2 = new Regex(@"^.*[bcdfghjklmnpqrstvwxyz][aeiou]([bcdfghjklmnpqrstvwxyz])\1ing$");
        public static Regex rgxI3 = new Regex(@"^.*ing$");
        public static Regex rgxS1 = new Regex(@"^.*s$");
        public static Regex rgxS2 = new Regex(@"^.*((s)|(z)|(x)|(ch)|(sh)|(o)|(y))es$");        
        public static Regex rgxS3 = new Regex(@"^.*[bcdfghjklmnpqrstvwxyz]ies$");
        public static Regex rgxS4 = new Regex(@"^.*ves$");
        public static Regex rgxP1 = new Regex(@"^.*[bcdfghjklmnpqrstvwxyz]ied$");
        public static Regex rgxP2 = new Regex(@"^.*[bcdfghjklmnpqrstvwxyz][aeiou]([bcdfghjklmnpqrstvwxyz])\1ed$");
        public static Regex rgxP3 = new Regex(@"^.*ed$");
    }
   
    /*
    - N V   
    - V N
    - N Adj V
    - N V N
    - V V N N
    - N N Adv V
    - Pro N V N
    - N V Pre N   
     */
    class GrammarTemplate
    {
        static WordType[][] GrammarTemplates;
        static GrammarTemplate()
        {
            GrammarTemplates = new WordType[8][];
            for (int i = 0; i < 2; i++)
            {
                GrammarTemplates[i] = new WordType[2];
            }
            for (int i = 2; i < 4; i++)
            {
                GrammarTemplates[i] = new WordType[3];
            }
            for (int i = 4; i < 7; i++)
            {
                GrammarTemplates[i] = new WordType[4];
            }
            // - N V 
            GrammarTemplates[0][0] = WordType.Noun;
            GrammarTemplates[0][1] = WordType.Verb;
            // - V N
            GrammarTemplates[1][0] = WordType.Verb;
            GrammarTemplates[1][1] = WordType.Noun;
            // - N Adj V
            GrammarTemplates[2][0] = WordType.Noun;
            GrammarTemplates[2][1] = WordType.Adjective;
            GrammarTemplates[2][2] = WordType.Verb;
            // - N V N
            GrammarTemplates[3][0] = WordType.Noun;
            GrammarTemplates[3][1] = WordType.Verb;
            GrammarTemplates[3][2] = WordType.Noun;
            // - V V N N
            GrammarTemplates[4][0] = WordType.Verb;
            GrammarTemplates[4][1] = WordType.Verb;
            GrammarTemplates[4][2] = WordType.Noun;
            GrammarTemplates[4][3] = WordType.Noun;
            // - N N Adv V
            GrammarTemplates[5][0] = WordType.Noun;
            GrammarTemplates[5][1] = WordType.Noun;
            GrammarTemplates[5][2] = WordType.Adverb;
            GrammarTemplates[5][3] = WordType.Verb;
            // - Pro N V N
            GrammarTemplates[6][0] = WordType.Pronoun;
            GrammarTemplates[6][1] = WordType.Noun;
            GrammarTemplates[6][2] = WordType.Verb;
            GrammarTemplates[6][3] = WordType.Noun;
            // - N V Pre N
            GrammarTemplates[7][0] = WordType.Noun;
            GrammarTemplates[7][1] = WordType.Verb;
            GrammarTemplates[7][2] = WordType.Preposition;
            GrammarTemplates[7][3] = WordType.Noun;
        }
    }
}

