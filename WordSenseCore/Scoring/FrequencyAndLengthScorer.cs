﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordSense.Dictionary;
using WordSense.Scoring;


namespace WordSense.Scoring
{
    class FrequencyAndLengthScorer : IIndividualScoreProvider
    {
        public double GetIndividualScore(string response, string context)
        {
            double currentDictionaryFactor = 0.6;
            IEnumerable<string> segments = response.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            double score = 1;
            segments.Foreach<string>(o =>
                score *= (((1 - currentDictionaryFactor) * (StaticDictionaries.UnigramDictionary.Count - StaticDictionaries.GetUnigramFrequency(o)) / StaticDictionaries.UnigramDictionary.Count)
                + currentDictionaryFactor * ((StaticDictionaries.CurrentWordFrequency.Count + 1) - StaticDictionaries.GetCurrentFrequency(o)) / (StaticDictionaries.CurrentWordFrequency.Count + 1)));
            return score;
        }
    }
}
