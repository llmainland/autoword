﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WordSense.MathParser
{
    class Constant
    {
        public static string[] SingleCharOperators = new[] { "+", "-", "*", "/", "^", "&", "|", "!", "~" };
        public static string[] PrefixUnaryOperators = new[] { "~", "cos", "sin", "tan", "cot", "sinh", "cosh", "tanh", "coth", "ln" };
        public static string[] SuffixUnaryOperators = new[] { "!" };
        public static string[] BinaryOperators = new[] { "+", "-", "*", "/", "^", "&", "|" };
        public static char LeftCompositor = '(';
        public static char RightCompositor = ')';
        public static string[] MultiCharacterConstants = new[] { "pi", "e" };
        public static string[] BinaryFunctionCalls = new[] { "log", "comb" };
        public static string[] SignExtension = new[] { "+", "-" };
        public static string[] BinaryHigherPrio = new[] { "*", "/", "&", "|" };
        public static string[] BinaryHighestPrio = new[] { "^" };
        //public static char AllowedDot = '.';
        public static string[] MultiCharOperators = new[] { "cos", "sin", "tan", "cot", "sinh", "cosh", "tanh", "coth", "log", "comb", "ln" };
        public static string Digits = "0123456789.,";
        public static char EOF = '@';

        public static Dictionary<string, double> PrecedenceAndValue;

        static Constant()
        {
            PrecedenceAndValue = new Dictionary<string, double>();
            foreach (var op in SignExtension) PrecedenceAndValue[op] = 0;
            foreach (var op in BinaryHigherPrio) PrecedenceAndValue[op] = 0.5;
            foreach (var op in BinaryHighestPrio) PrecedenceAndValue[op] = 0.75;
            foreach (var op in PrefixUnaryOperators) PrecedenceAndValue[op] = 1;
            foreach (var op in SuffixUnaryOperators) PrecedenceAndValue[op] = 2;
            foreach (var op in BinaryFunctionCalls) PrecedenceAndValue[op] = 3;
            PrecedenceAndValue["pi"] = Math.PI;
            PrecedenceAndValue["e"] = Math.E;
        }
    }
    /// <summary>
    /// a mutual exclusive type of tokens.
    /// </summary>
    enum TokenType
    {
        Whitespace,
        Compositors,
        Operands,
        SingleCharOperators,
        Initial,
        Unrecognized,
        MultiCharOperators,
        EOF
    }
    public static class MathEvaluation
    {
        //ugly but most memory conservative.
        static LinkedList<T> toQueue<T>(IEnumerable<T> q)
        {
            var list = new LinkedList<T>();
            foreach (var item in q) list.AddLast(item);
            return list;
        }
        /// <summary>
        /// ultimately ugly.
        /// </summary>
        /// <param name="operatorStack"></param>
        /// <param name="operandStack"></param>
        /// <param name="dynamicPriorityStack"></param>
        /// <param name="finalResult"></param>
        /// <returns></returns>
        static bool finalize(Stack<string> operatorStack, Stack<double> operandStack, Stack<double> dynamicPriorityStack, out double finalResult)
        {
            finalResult = -1;
            //dynamic priority stack should be non-decreasing.
            if (operatorStack.Count != 0 && operandStack.Count == 0) return false;
            while (operatorStack.Count > 0)
            {
                //more things to be done.
                //if the precedence is not equal, then the largest one must be the last one.
                if (2 > dynamicPriorityStack.Max() && dynamicPriorityStack.Max() == dynamicPriorityStack.Min())
                {
                    //INVARIANT: should be called ONLY ONCE.
                    //inverse the stacks so evaluation is from left to right.
                    //solve internally.
                    //soooooooo ugly.any better ideas??
                    var operatorQ = toQueue<string>(operatorStack.Reverse());
                    var operandQ = toQueue<double>(operandStack.Reverse());
                    operatorStack.Clear(); operandStack.Clear();
                    var precedence = dynamicPriorityStack.Max(); ;// dynamicPriorityStack.Pop();
                    while (operatorQ.Count > 0)
                    {
                        var op = operatorQ.First.Value;
                        operatorQ.RemoveFirst();
                        var result = 0.0;
                        if (Constant.BinaryOperators.Contains(op) && precedence < 2)
                        {
                            var op2 = operandQ.First.Value;
                            operandQ.RemoveFirst();
                            var op1 = operandQ.First.Value;
                            operandQ.RemoveFirst();
                            result = resolve(op, op1, op2, precedence);
                        }
                        else
                        {
                            var op1 = operandQ.First.Value;
                            operandQ.RemoveFirst();
                            //unary. or priority elevated.
                            result = resolve(op, op1, 0, precedence);
                        }
                        operandQ.AddFirst(result);
                    }
                    //sigh,,,,
                    finalResult = operandQ.First.Value;
                    operandQ.RemoveFirst();
                    return operandQ.Count + operatorQ.Count == 0;
                }
                else
                {
                    var op = operatorStack.Pop();
                    var precedence = dynamicPriorityStack.Pop();
                    var result = 0.0;
                    if (Constant.BinaryOperators.Contains(op) && precedence < 2)
                    {
                        result = resolve(op, operandStack.Pop(), operandStack.Pop(), precedence);
                    }
                    else
                    {
                        //unary. or priority elevated.
                        result = resolve(op, operandStack.Pop(), 0, precedence);
                    }
                    operandStack.Push(result);
                }
            }
            finalResult = operandStack.Pop();
            return operatorStack.Count + operandStack.Count == 0;
        }

        static int searchForPairedRightCompositor(string formula, int startIdx)
        {
            int encounterLeft = 1;
            for (int i = startIdx; i < formula.Length; i++)
            {
                if (formula[i] == Constant.LeftCompositor) encounterLeft++;
                else if (formula[i] == Constant.RightCompositor)
                {
                    encounterLeft--;
                    if (encounterLeft == 0)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        static bool attemptResolve(Stack<string> operatorStack, Stack<double> operandStack, Stack<double> dynamicPriorityStack, string currentOp, double currentOpPriority)
        {
            if (operatorStack.Count == 0)
            {
                return operandStack.Count <= 1;
            }
            while (operatorStack.Count > 0 && currentOpPriority < dynamicPriorityStack.Peek())
            {
                var op = operatorStack.Pop();
                var precedence = dynamicPriorityStack.Pop();
                var result = 0.0;
                if (Constant.BinaryOperators.Contains(op) && precedence < 2)
                {
                    result = resolve(op, operandStack.Pop(), operandStack.Pop(), precedence);
                }
                else
                {
                    //unary. or priority elevated.
                    result = resolve(op, operandStack.Pop(), 0, precedence);
                }
                operandStack.Push(result);
            }
            return true;
        }
        static string getLongestMatchedSymbol(string formula, string currToken, int i, IEnumerable<string> symbols2Compare)
        {
            string longestMatchedSymbol = "";
            //see what we have now.
            foreach (var symbol in symbols2Compare)
            {
                if (symbol.StartsWith(currToken.ToString()))
                {
                    //it could match longer.
                    //look ahead.
                    if (i - currToken.Length + symbol.Length >= formula.Length) continue;//cannot possibly match.
                    var subStr = formula.Substring(i - currToken.Length + 1, symbol.Length);
                    if (subStr == symbol && symbol.Length > longestMatchedSymbol.Length)
                    {
                        longestMatchedSymbol = symbol;
                    }
                }
            }
            return longestMatchedSymbol;
        }

        /// <summary>
        /// Rewrite with System. Reflection to automate this. Disgusting --liang
        /// </summary>
        /// <param name="op"></param>
        /// <param name="operand1"></param>
        /// <param name="operand2"></param>
        /// <param name="dynamicPriority"></param>
        /// <returns></returns>
        static double resolve(string op, double operand1, double operand2, double dynamicPriority, bool flipOperand = false)
        {
            var firstOperand = operand1;
            if (flipOperand)
            {
                var temp = operand1;
                operand1 = operand2;
                operand2 = temp;
            }
            switch (op)
            {
                case "log":
                    return Math.Log(operand2, operand1);
                case "-":
                    if (dynamicPriority == 0)
                    {
                        return operand2 - operand1;
                    }
                    else
                    {
                        return -1 * firstOperand;
                    }
                case "+":
                    if (dynamicPriority == 0)
                    {
                        return operand1 + operand2;
                    }
                    else
                    {
                        return firstOperand;
                    }
                case "!":
                    {
                        double accumulate = 1.0;
                        for (int i = 1; i <= (int)firstOperand; i++)
                        {
                            accumulate *= i;
                        }
                        return accumulate;
                    }
                case "comb":
                    {
                        return resolve("!", operand1, 0, 0) / (resolve("!", operand2, 0, 0) * resolve("!", operand1 - operand2, 0, 0));
                    }
                case "|":
                    {
                        return (long)operand1 | (long)operand2;
                    }
                case "~":
                    {
                        return ~(long)firstOperand;
                    }
                case "&":
                    {
                        return (long)operand1 & (long)operand2;
                    }
                case "*":
                    {
                        return operand1 * operand2;
                    }
                case "/":
                    {
                        return operand2 / operand1;
                    }
                case "tan":
                    {
                        return Math.Tan(firstOperand);
                    }
                case "cot":
                    {
                        return 1 / Math.Tan(firstOperand);
                    }
                case "sin":
                    {
                        return Math.Sin(firstOperand);
                    }
                case "cos":
                    {
                        return Math.Cos(firstOperand);
                    }
                case "sinh":
                    {
                        return Math.Sinh(firstOperand);
                    }
                case "cosh":
                    {
                        return Math.Cosh(firstOperand);
                    }
                case "tanh":
                    {
                        return Math.Tanh(firstOperand);
                    }
                case "coth":
                    {
                        return 1 / Math.Tanh(firstOperand);
                    }
                case "^":
                    {
                        return Math.Pow(operand2, operand1);
                    }
                default:
                    throw new InvalidOperationException("Bug check resolve().");
            }
        }

        public static bool TryEval(string formula, out double result)
        {
            var operatorStack = new Stack<string>();
            var operandStack = new Stack<double>();
            var dynamicOperatorPriority = new Stack<double>();//some operators can change priority, eg the prefix - at the beginning.
            var lastTokenType = TokenType.Initial;
            var currToken = new StringBuilder();
            result = -1;
            //formula = string.Format("{0}{1}", formula, Constant.EOF);//@ as EOF.
            for (int i = 0; i < formula.Length; i++)
            {
                var currTokenType = TokenType.Unrecognized;
                var currChar = formula[i];
                currToken.Append(currChar);
                if (Constant.SingleCharOperators.Contains(currChar.ToString()) || Constant.SuffixUnaryOperators.Contains(currChar.ToString()) || Constant.PrefixUnaryOperators.Contains(currChar.ToString()))
                {
                    currTokenType = TokenType.SingleCharOperators;
                }
                else if (Constant.Digits.Contains(currChar.ToString()))
                {
                    //also look ahead.
                    for (int j = i + 1; j < formula.Length; j++)
                    {
                        if (Constant.Digits.Contains(formula[j]) == false)
                        {
                            i = j - 1;//apparently token stops somewhere before. -1 because the ++ of i in main loop.
                            break;
                        }
                        currToken.Append(formula[j]);
                        i++;
                    }
                    currTokenType = TokenType.Operands;
                }
                else if (Constant.LeftCompositor == currChar)
                {
                    //treat this as an operand by resolving that part first.
                    int rightCompositor = searchForPairedRightCompositor(formula, i + 1);
                    if (rightCompositor == -1) return false;
                    //solve that part now.
                    var substr = formula.Substring(i + 1, rightCompositor - i - 1);
                    var partial = 0.0;
                    if (!TryEval(substr, out partial)) return false;
                    currToken.Clear();
                    currToken.Append(partial);
                    currTokenType = TokenType.Operands;
                    //adjust i.
                    i = rightCompositor;
                }
                else if (Constant.EOF == currChar)
                    currTokenType = TokenType.EOF;
                else currTokenType = TokenType.Unrecognized;

                //same as before.

                //if transitioned from unrecognized to something else, the previous unrecognized token could have been an operator
                if (currTokenType == TokenType.Unrecognized)
                {
                    var symbol = getLongestMatchedSymbol(formula, currToken.ToString(), i, Constant.MultiCharacterConstants);
                    if (symbol != "")
                    {
                        //commit this, and advance i.
                        currTokenType = TokenType.Operands;
                        i = i + symbol.Length - currToken.Length;
                        currToken.Clear();
                        currToken.Append(symbol);
                    }
                    else
                    {
                        if ("" != (symbol = getLongestMatchedSymbol(formula, currToken.ToString(), i, Constant.MultiCharOperators)))
                        {
                            currTokenType = TokenType.MultiCharOperators;
                            i = i + symbol.Length - currToken.Length;
                            currToken.Clear();
                            currToken.Append(symbol);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                //cross token boundry, or resolved a multichar symbol.
                //flush. operand is the easy case.
                if (currTokenType == TokenType.Operands)
                {
                    double evalResult;
                    if (double.TryParse(currToken.ToString(), out evalResult))
                    {
                        operandStack.Push(evalResult);
                    }
                    else if (currToken.ToString() == "e")
                    {
                        operandStack.Push(Math.E);
                    }
                    else if (currToken.ToString() == "pi")
                    {
                        operandStack.Push(Math.PI);
                    }
                    else return false;
                }
                else
                {
                    //is an operator. Which kind of operator? is it a function call?
                    // a function call must be resolved first.
                    if (Constant.BinaryFunctionCalls.Contains(currToken.ToString()))
                    {
                        //is a function call. damn.
                        //recur. resolve both parameters then put result directly into operand stack.
                        if (formula.Length - 2 < i || formula[i + 1] != Constant.LeftCompositor || i + 2 >= formula.Length) return false;//no call.
                        int RightCompositorIdx = searchForPairedRightCompositor(formula, i + 2);
                        if (RightCompositorIdx == -1) return false;//no call.
                        var parameters = formula.Substring(i + 2, RightCompositorIdx - i - 2);//this is the call, now split.
                        var splitPt = parameters.IndexOf(',');
                        if (splitPt == -1) return false;//invalid # of argument
                        double leftResult, rightResult;
                        bool leftOkay = TryEval(parameters.Substring(0, splitPt), out leftResult);
                        bool rightOkay = TryEval(parameters.Substring(parameters.IndexOf(',') + 1, parameters.Length - splitPt - 1), out rightResult);
                        if (leftOkay == false || rightOkay == false) return false;//something is wrong.
                        var partial = resolve(currToken.ToString(), leftResult, rightResult, 3);
                        //this should go directly into the operand stack as they are function calls.
                        operandStack.Push(partial);
                        //change i.
                        i = RightCompositorIdx;
                    }
                    else
                    {
                        double priority = Constant.PrecedenceAndValue[currToken.ToString()];
                        priority = /*is prefix? elevate priority if yes*/ lastTokenType == TokenType.Initial && Constant.SignExtension.Contains(currToken.ToString()) ? 2 : priority;
                        if (!attemptResolve(operatorStack, operandStack, dynamicOperatorPriority, currToken.ToString(), priority)) return false;//something wrong poping stacks.
                        operatorStack.Push(currToken.ToString());
                        dynamicOperatorPriority.Push(priority);
                        //should we at least try to resolve some?
                    }
                }
                currToken.Clear();
                lastTokenType = currTokenType;
            }
            return finalize(operatorStack, operandStack, dynamicOperatorPriority, out result);
        }
    }
}
