﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordSense.TextParsing
{
    public class Constants
    {
        public static char[] Delimiters = { '.', ',', Space, CarriageReturn, LineFeed, '!', '?', '@', '+', '-', '*', '/' };
        public static char[] SentenceDelimiters { get { return EndOfSentence; } }
        public static char[] ParagraphDelimiters = { LineFeed, CarriageReturn };
        public const char LineFeed = '\n';
        public const char CarriageReturn = '\r';
        public static int MaximumAllowedRequestsPerKeyPress = 16;
        public const char Space = ' ';
        public const char DecimalPoint = '.';
        public const char Equal = '=';
        public const string AllowedCharactersInFormula = "1234567890-=*/.()+^,&|!~";
        public static char[] Operators = { '+', '-', '/', '*', '^', '&', '|', '!', '~' };
        public static char[] OtherSigns = { '=', '(', ')', '.', ',' };
        public static char[] Digits = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'e' };
        public static char[] NumDigits = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
        public const char EOS = '.';
        public static string[] MathConstants = { "pi" };
        public static string[] SupportedOperations = { "cos", "sin", "tan", "cot", "sinh", "cosh", "tanh", "coth", "ln", "log", "comb" };
        public static char[] EndOfSentence = { '.', ';', '!', ',', ':', '!', '?','\r','\n'};
        public const string SpaceString = " ";
    }
}
