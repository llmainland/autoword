﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordSense.TextParsing
{
    /// <summary>
    /// Not sure what the complete set would be.
    /// </summary>
    [Flags]
    public enum TextCategory : uint
    {
        NonEntity = 1,
        Noun = 2,
        Verb = 4,
        Conjunct = 8,
    }
}
