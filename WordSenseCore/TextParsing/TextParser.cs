﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace WordSense.TextParsing
{
    public class TextProcessing
    {
        /// <summary>
        /// Gets the last sentence.
        /// </summary>
        /// <param name="document">Document to process</param>
        /// <param name="position">current editing position</param>
        /// <param name="countingCurrent">Ignores the trailing EOS. If true, then this is effectively finding the last sentence. Otherwise, finds the current sentence.</param>
        /// <returns></returns>
        public static string GetLastSentence(string document, int position, bool countingCurrent = true)
        {
            if (position == 0) return Constants.EndOfSentence.Contains(document[0]) ? string.Empty : document.Substring(0, 1);
            int EOSLstIdx = document.LastIndexOfAny(Constants.EndOfSentence, position);
            // int EOSPUlIdx = document.LastIndexOfAny(Constants.EndOfSentence, EOSLstIdx == 0 ? 0 : EOSLstIdx - 1);
            if (countingCurrent || EOSLstIdx < 0) return document.Substring(EOSLstIdx + 1, position - EOSLstIdx);//there is no preceding EOS,
            //EOS is at the beginning or somewhere further, but that's okay
            //just get the more previous one.
            if (EOSLstIdx == 0) return string.Empty;
            int EOSPulIdx = document.LastIndexOfAny(Constants.EndOfSentence, EOSLstIdx - 1);
            return document.Substring(EOSPulIdx + 1, EOSLstIdx - EOSPulIdx - 1);
            //int idx = 0;
            //for (int i = position - 1; i >= 0; i--)
            //{
            //    if (!Constants.EndOfSentence.Contains(document[i]))
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        if (Constants.NumDigits.Contains(document[i + 1]))
            //        {
            //            continue;
            //        }
            //        else
            //        {
            //            idx = i;
            //            break;
            //        }
            //    }
            //}
            //if (Constants.EndOfSentence.Contains(document[idx]))
            //{
            //    idx++; //idx+1 is at most position.
            //}
            //String result = document.Substring(idx, position - idx + 1);
            //result = result.TrimStart();
            //return result;
        }

        public static bool TryRetrieveFormula(string document, int position, out string formula, out int start)
        {
            Boolean lastIsNumber = false;
            Boolean leftIsNumber = false;
            Boolean hasNumber = false;
            Boolean hasOperator = false;
            Boolean possibleExpression = false;
            int commaPosition = 0;
            formula = string.Empty;
            start = -1;
            if (document[position] != Constants.Equal)
            { // latest input must be a "="
                return false;
            }
            int i = position - 1;
            while (i >= 0)
            {
                possibleExpression = false;
                if (document[i] == ' ')
                { // tempWordType
                    start = i;
                    i--;
                    continue;
                }
                if (Constants.Digits.Contains(document[i]))
                { // digit
                    start = i;
                    i--;
                    lastIsNumber = true;
                    leftIsNumber = true;
                    hasNumber = true;
                    continue;
                }
                if (Constants.Operators.Contains(document[i]))
                { // operator
                    start = i;
                    i--;
                    lastIsNumber = false;
                    if (i >= 0 && (document[i] == '+' || document[i] == '-' || document[i] == '~'))
                    {
                        lastIsNumber = true;
                    }
                    hasOperator = true;
                    continue;
                }
                if (Constants.OtherSigns.Contains(document[i]))
                { // non-operator, .()=
                    if (document[i] == ',')
                    {
                        if (lastIsNumber)
                        {
                            start = i;
                            commaPosition = i;
                            lastIsNumber = false;
                            i--;
                            continue;
                        }
                        else
                        {
                            start = i + 1;
                            break;
                        }
                    }
                    start = i;
                    lastIsNumber = false;
                    i--;
                    continue;
                }
                if (document[i] == 'i' && i != 0 && (document[i - 1] == 'p'))
                { // pi as a number
                    start = i - 1;
                    i -= 2;
                    lastIsNumber = true;
                    hasNumber = true;
                    continue;
                }
                int endOperation = i;
                i--;
                string tempOperation = "";
                while (i >= 0 && tempOperation.Length <= 4 && !Constants.AllowedCharactersInFormula.Contains(document[i]))
                { // multi-char operations
                    tempOperation = document.Substring(i, endOperation - i + 1);
                    i--;
                    tempOperation = tempOperation.Replace(" ", "");
                    if (Constants.SupportedOperations.Contains(tempOperation))
                    {
                        start = i + 1;
                        lastIsNumber = true;
                        hasOperator = true;
                        possibleExpression = true;
                        break;
                    }
                }
                if (!possibleExpression)
                {
                    break;
                }
            }
            if (hasNumber && hasOperator)
            { // at least a number and an operator
                formula = document.Substring(start, position - start);
                formula = formula.Replace(" ", "");
                if (formula[0] == ',')
                {
                    start = commaPosition + 1;
                    formula = formula.Substring(1, formula.Length - 1);
                }
                return true;
            }
            return false;
        }

        public static string[] SplitDocument(string document, TextUnit splitUnit)
        {
            var splittingCharacters = new List<char>();
            //fast.
            if (splitUnit <= TextUnit.Paragraph)
            {
                splittingCharacters.AddRange(Constants.ParagraphDelimiters);
            }
            if (splitUnit <= TextUnit.Sentence)
            {
                splittingCharacters.AddRange(Constants.SentenceDelimiters);
            }
            /*if(splitUnit <= TextUnit.Word)
            {
                splittingCharacters.AddRange(Constants.Delimiters);
            }*/
            //now split.
            return document.Split(splittingCharacters.ToArray(), StringSplitOptions.RemoveEmptyEntries);

        }
        [Flags]
        /// <summary>
        /// Expand current character to the minimum unit encompassing it.
        /// </summary>
        public enum TextUnit : uint
        {
            Paragraph = 1,
            Word = 2,
            Sentence = 3
        }
        /// <summary>
        /// Expands the current character to the unit specified
        /// </summary>
        /// <param name="cursorPosition"></param>
        /// <param name="document"></param>
        /// <param name="expansionUnit"></param>
        /// <param name="unitStartPosition">0 or the beginning of the expanded unit, inclusive</param>
        /// <param name="unitEndPosition">end of string or the end of the expanded unit, inclusive</param>
        public static void Expand(int cursorPosition, string document, TextUnit expansionUnit, out int unitStartPosition, out int unitEndPosition)
        {
            //if (expansionUnit != TextUnit.Paragraph) throw new NotImplementedException("expansionUnit must be Paragraph in this build");
            var separator = new[] { Constants.Space };
            switch (expansionUnit)
            {
                case TextUnit.Paragraph:
                    {
                        separator = Constants.ParagraphDelimiters;
                        break;
                    }
                case TextUnit.Sentence:
                    {
                        separator = Constants.SentenceDelimiters;
                        break;
                    }
                default:
                    break;
            }
            unitStartPosition = document.LastIndexOfAny(separator, cursorPosition);
            if (unitStartPosition == -1) unitStartPosition = 0;
            unitEndPosition = document.LastIndexOfAny(separator, cursorPosition);
            if (unitEndPosition == -1) unitEndPosition = document.Length - 1;
        }
        public static string[] Tokenize(string textContent)
        {
            return textContent.Split(Constants.Delimiters, StringSplitOptions.RemoveEmptyEntries);
        }

        public static TextCategory ParseText(string text)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets up to count words (inclusive) from the current position. If a delimiter or the start of document is encountered, the procedure ends.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="position"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string[] GetPrecedingWords(string document, int position, int count, out int[] indices)
        {
            var result = new List<string>();
            //if (string.IsNullOrWhiteSpace( document[position].ToString()) && document[position + 1] == Constants.CarriageReturn)
            //{
            //    indices = new int[0];
            //    return result.ToArray();
            //}
            var accumulator = new StringBuilder();
            var indicesCandidates = new List<int>();
            for (int cursor = position; cursor >= 0; cursor--)
            {
                if (!Constants.Delimiters.Contains(document[cursor]))
                {
                    accumulator.Insert(0, document[cursor]);
                }
                else
                {
                    if (accumulator.Length != 0)
                    {
                        result.Add(accumulator.ToString());
                        accumulator.Clear();
                        indicesCandidates.Add(cursor + 1);
                    }
                    if (result.Count == count || document[cursor] != Constants.Space) break;//finished.
                }
            }
            if (accumulator.Length != 0)
            {
                indicesCandidates.Add(0);
                result.Add(accumulator.ToString());
            }
            Debug.Assert(result.Count <= count);
            indices = indicesCandidates.ToArray();
            return result.ToArray();
        }

        public static string GetCurrentWord(string document, int position)
        {
            int[] notUsed;
            if (Constants.Delimiters.Contains(document[position])) return string.Empty;
            return GetPrecedingWords(document, position, 1, out notUsed)[0];
        }

        internal static bool AllCapitalized(string p)
        {
            return p.All<char>(ch => Char.ToUpperInvariant(ch) == ch);
        }
    }

}
