﻿using WordSense.Dictionary;
using WordSense.Trie;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace WordSense.WorkItem
{
    public class BigramDoubleWorkload : Workload
    {
        public BigramDoubleWorkload(long requestId, long editingStart, string[] parameters, string context) : base(requestId, editingStart, parameters, context) { }



        public override Task<WorkResult> Execute()
        {
            var task = Task<WorkResult>.Run(() =>
            {
                var result = new WorkResult(editingStartPosition, this);
                if (String.IsNullOrEmpty(Parameters[0])) return result;

                var array = StaticDictionaries.BigramDictionary.GetByPrefix(Parameters[0]);
                array = array.OrderBy<TrieEntry<int>, int>(o => o.Value);
                var list = array.Select<TrieEntry<int>, string>(o => o.Key).ToList();
                list.Remove(Parameters[0]);
                result.AddCandidates(list.Take(2));

                //result.AddCandidates(array.Take(2).Select<TrieEntry<int>, string>(o => o.Key));
                //WorkResult.Add(result);

                // 0.75 - 0.95
                if (list.Count() == 1)
                {
                    var fullLen = result.Results[0].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[0] = 0.9 + 0.05 * fullLen / 12 + 0.02 * Parameters[0].Length / fullLen;
                }
                else if (list.Count() > 1)
                {
                    var fullLen = result.Results[0].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[0] = 0.85 + 0.05 * fullLen / 12 + 0.02 * Parameters[0].Length / fullLen;

                    fullLen = result.Results[1].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[1] = 0.75 + 0.05 * fullLen / 12 + 0.02 * Parameters[0].Length / fullLen;
                }

#if PERF
                Tock();
#endif
                return result;
            });
            return task;
        }
    }
}
