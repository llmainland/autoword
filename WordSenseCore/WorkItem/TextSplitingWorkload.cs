﻿using WordSense.Dictionary;
using WordSense.TextParsing;
using WordSense.Trie;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WordSense.WorkItem
{
    public class TextSplitingWorkload : Workload
    {
        /// <summary>
        /// CLR Does not guarentee atomic writes in dictionaries. Use concurrent versions.
        /// </summary>
        static ConcurrentDictionary<int, Trie<int>> hash2StrArray;
        static ConcurrentDictionary<int, long> hash2ReqId;
        static TextSplitingWorkload()
        {
            hash2ReqId = new ConcurrentDictionary<int, long>();
            hash2StrArray = new ConcurrentDictionary<int, Trie<int>>();
        }

        public TextSplitingWorkload(long requestId, long editingStart, string[] parameters, string context) : base(requestId, editingStart, parameters,context)
        {
            ResultPropagation = ResultPropagationType.Individual;
        }

        public override Task<WorkResult> Execute()
        {
            //hand off now.
            //can we devise a way that is lock free?
            var task =Task<WorkResult>.Run(() =>
            {
                var doc = Parameters[0];
                var paragraphs = TextProcessing.SplitDocument(doc, TextProcessing.TextUnit.Paragraph);
                var results = new WorkResult(editingStartPosition,this);
                var list = StaticDictionaries.KeywordDictionary.GetByPrefix(Parameters[1]).Select<TrieEntry<int>, string>(o => o.Key).ToList();
                //list.Remove(Parameters[0]);
                results.AddCandidates(list);

                // candidatelist words should have low score if they are in dictionary

                int numWords = 0;
                if (results.Results.Count == 1)
                {
                    var fullLen = results.Results[0].Length;
                    if (fullLen > 12) fullLen = 12;
                    results.IndividualScores[0] = 0.95 + 0.05 * fullLen / 12;
                    numWords++;
                }

                var candidateList = new List<string>();
                var countList = new List<int>();
                var entryList = new List<TrieEntry<int>>();
                foreach (var paragraph in paragraphs)
                {
                    if (TaskObsolete) return results;
                    var hash = paragraph.GetHashCode();
                    Trie<int> values;
                    //Interlocked.CompareExchange()
                    hash2ReqId[hash] = RequestId;//should do compare exchange. But not supported in dictionary.
                    if (!hash2StrArray.TryGetValue(hash, out values))
                    {
                        values = new Trie<int>();
                        // (task obsoletes \/ not calculated)
                        if (TaskObsolete) return results;
                        // (task obsoletes \/ not calculated)
                        //ok if task obsoletes just now.
                        var cap_para = TextProcessing.Tokenize(paragraph).Where<string>(str => str.Length >= 5).GroupBy<string, string>(o => o).Select<IGrouping<string, string>, TrieEntry<int>>(o => new TrieEntry<int>(o.Key, o.Count()));
                        cap_para.Foreach<TrieEntry<int>>(o => {
                            values.Add(o);
                            //values.Add(new TrieEntry<int>(o.Key.ToLowerInvariant(), o.Value));
                        });

                        hash2StrArray[hash] = values;
                        //now make the news available to all threads.
                     
                    }

                    entryList.AddRange(hash2StrArray[hash].GetByPrefix(Parameters[1]).Where<TrieEntry<int>>(o => o.Key != Parameters[1]));
                    //I'm the owner of the key now.

                }
                //clean up useless keys. CLAIM: all associated keys with reqid less than current are guarenteed to be garbage. Not considering undoing.
                foreach (var key in hash2ReqId.Keys)
                {
                    long reqId;
                    Trie<int> values;
                    if (hash2ReqId.TryGetValue(key, out reqId))
                    {
                        if (reqId < RequestId)
                        {
                            //we did not use 
                            hash2ReqId.TryRemove(key, out reqId);
                            hash2StrArray.TryRemove(key, out values);
                        }
                    }
                }

                entryList = entryList.GroupBy<TrieEntry<int>, string>(o => o.Key)
                    .Select<IGrouping<string, TrieEntry<int>>, TrieEntry<int>>(o => new TrieEntry<int>(o.Key, o.Sum<TrieEntry<int>>(p => p.Value)))
                    .OrderByDescending<TrieEntry<int>, int>(o => o.Value).ToList();
                candidateList = entryList.Select<TrieEntry<int>, string>(o => o.Key).ToList();
                countList = entryList.Select<TrieEntry<int>, int>(o => o.Value).ToList();

                if (candidateList.Count != 0)
                {
                    if (numWords != 0)
                    {
                        candidateList.Remove(results.Results[0]);
                    }
                    List<int> counts = countList.Take(2).ToList();
                    if (counts.Count == 2 && counts[0] == counts[1])
                    {
                        results.AddCandidates(candidateList.Take(2).OrderByDescending<string, int>(str => wordFreq(str)));
                    }
                    else results.AddCandidates(candidateList.Take(2));
                    //WorkResult.Add(results);

                    if (results.Results.Count() - numWords == 1)
                    {
                        var fullLen = results.Results[numWords].Length;
                        if (fullLen > 12) fullLen = 12;
                        if (counts[0] > 8) counts[0] = 8;
                        results.IndividualScores[0] = 0.85 + 0.05 * fullLen / 12 + 0.01 * counts[0];
                    }
                    else if (results.Results.Count() - numWords > 1)
                    {
                        var fullLen = results.Results[numWords].Length;
                        if (fullLen > 12) fullLen = 12;
                        if (counts[0] > 8) counts[0] = 8;
                        results.IndividualScores[numWords] = 0.85 + 0.05 * fullLen / 12 + 0.01 * counts[0];

                        fullLen = results.Results[numWords + 1].Length;
                        if (fullLen > 12) fullLen = 12;
                        if (counts[1] > 8) counts[1] = 8;
                        results.IndividualScores[numWords + 1] = 0.80 + 0.05 * fullLen / 12 + 0.01 * counts[1];
                    }


                    //for (int i = 0; i < results.Results.Count; i++)
                    //{
                    //    results.IndividualScores[i] = (results.IndividualScores[i] - 0.6) * wordFreq(results.Results[i]) / 20000 + 0.6;
                    //    var curlen = Parameters[1].Length;
                    //    if (curlen > 5) curlen = 5;
                    //    results.IndividualScores[i] = (results.IndividualScores[i] - 0.6) * curlen / 5 + 0.6;
                    //}
                }

                
#if PERF
                Parameters[0] = Parameters[0].Substring(0, (int)Math.Min(10, Parameters[0].Length));
                Tock();
#endif
                return results;
            });
            return task;
        }


        private int wordFreq(string str)
        {
            int value;
            bool x = StaticDictionaries.UnigramDictionary.TryGetValue(str, out value);
            if (x)
            {
                return value;
            }
            else
            {
                return StaticDictionaries.UnigramDictionary.Count;
            }
        }
    }
}
