﻿using WordSense.Dictionary;
using WordSense.Trie;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Data.Json;
using WordSense.Scoring;

namespace WordSense.WorkItem
{
    public class BigramWorkload : Workload
    {
        public BigramWorkload(long requestId, long editingStart, string[] parameters, string context) : base(requestId, editingStart, parameters, context) { }



        public override Task<WorkResult> Execute()
        {
            var task = Task<WorkResult>.Run(() =>
            {
                var result = new WorkResult(editingStartPosition, this);
                if (String.IsNullOrEmpty(Parameters[0])) return result;
                var array = StaticDictionaries.BigramDictionary.GetByPrefix(Parameters[0]);
                array = array.OrderByDescending<TrieEntry<int>, double>(o => o.Value * PostResultScorer.Instance.GetIndividualScore(o.Key, ScoringCriterion.Grammar | ScoringCriterion.Coherence, Context)).Take(2);
                var list = array.Select<TrieEntry<int>, string>(o => o.Key).ToList();
                list.Remove(Parameters[0]);
                result.AddCandidates(list.Take(2));

                //result.AddCandidates(array.Take(2).Select<TrieEntry<int>, string>(o => o.Key));
                //WorkResult.Add(result);

                // 0.65 - 0.9
                if (list.Count() == 1)
                {
                    var fullLen = result.Results[0].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[0] = 0.85 + 0.05 * fullLen / 12 + 0.03 * (StaticDictionaries.BigramDictionary.Count - StaticDictionaries.GetBigramFrequency(result.Results[0])) / StaticDictionaries.BigramDictionary.Count;
                }
                else if (list.Count() > 1)
                {
                    var fullLen = result.Results[0].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[0] = 0.78 + 0.05 * fullLen / 12 + 0.03 * (StaticDictionaries.BigramDictionary.Count - StaticDictionaries.GetBigramFrequency(result.Results[0])) / StaticDictionaries.BigramDictionary.Count;

                    fullLen = result.Results[1].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[1] = 0.75 + 0.05 * fullLen / 12 + 0.03 * (StaticDictionaries.BigramDictionary.Count - StaticDictionaries.GetBigramFrequency(result.Results[1])) / StaticDictionaries.BigramDictionary.Count;
                }


#if PERF
                Tock();
#endif
                return result;
            });
            return task;
        }
    }
}
