﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordSense.WorkItem
{
    public class Constants
    {
        public const int MaximumAllowedRequestsPerKey = 16;
        public const int MaximumAllowedRetrivalDelayMS = 100;
    }
}
