﻿using WordSense.Dictionary;
using System;
using System.Collections.Generic;
using System.Text;

namespace WordSense.WorkItem
{
    public class WorkloadProducingLongResultsAttribute : WorkloadAttribute { }

    [WorkloadProducingLongResults]
    public  class intelliSenseWorkload : Workload
    {
        public intelliSenseWorkload(long groupId, long editingStart, string[] parameters)
            : base(groupId, editingStart, parameters)
        {
        }
        public async override System.Threading.Tasks.Task<WorkResult> Execute()
        {
            WorkResult wr = new WorkResult(editingStartPosition, this);
            StringBuilder sb = new StringBuilder();
            Parameters[0] = Parameters[0].Replace("'s", "");
            for (int i = 0; i < Parameters.Length; i++)
            {
                sb.Insert(0, " ");
                sb.Insert(0, Parameters[i]);
                if (StaticDictionaries.EntityDictionary.ContainsKey(sb.ToString().TrimEnd()))
                {
                    wr.AddCandidates(StaticDictionaries.EntityDictionary[sb.ToString().TrimEnd()].RelatedContent);
                    return wr;
                }
            }
            return wr;
        }
    }
}
