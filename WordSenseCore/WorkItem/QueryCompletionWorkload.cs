﻿using WordSense.Dictionary;
using WordSense.Scoring;
using WordSense.TextParsing;
using WordSense.Trie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace WordSense.WorkItem
{
    public class QueryCompletionWorkload : Workload
    {
        static string queryUrl = "http://suggestqueries.google.com/complete/search?client=firefox&q=";
        static HttpClient client = new HttpClient();
        static string createUri(string parameter)
        {
            return string.Format("{0}{1}", queryUrl, parameter);
        }
        public QueryCompletionWorkload(long requestId, long editingStart, string[] parameters, string context) : base(requestId, editingStart, parameters, context)
        {
            Prerequisites = LaunchPrerequisites.NetworkAvaiable;
            ResultPropagation = ResultPropagationType.Group;
        }

        public async override Task<WorkResult> Execute()
        {
            //StaticDictionaries.BigrammWordDictionary;
            var workResult = new WorkResult(editingStartPosition, this);

            string result = null;
            var param = string.Join(TextParsing.Constants.Space.ToString(), Parameters).ToLowerInvariant();
            if (param.Trim() == string.Empty) return workResult;
            var uri = createUri(param);
            result = await (await client.GetAsync(uri)).Content.ReadAsStringAsync();
            JsonArray responseArray;
            if (TaskObsolete) return workResult;
            if (JsonArray.TryParse(result, out responseArray))
            {
                var fullResults = responseArray.GetArrayAt(1).GetArray().Select<IJsonValue, string>(v => v.GetString()).Where<string>(str => str.ToLowerInvariant() != param && str.ToLowerInvariant().StartsWith(param));
                var trimmed = fullResults.Select<string, string[]>(str => str.Split(' ')).Where<string[]>(strs => strs.Length >= Parameters.Length).Select<string[], string>(strs => string.Join(" ", strs.Take(Parameters.Length))).Where<string>(str => str.ToLowerInvariant() != param).Distinct();
                fullResults = fullResults.Concat<string>(trimmed);
                var rankings = fullResults.OrderByDescending<string, double>(str => PostResultScorer.Instance.GetIndividualScore(str, ScoringCriterion.FrequencyAndLength | ScoringCriterion.Coherence, Context)).Distinct();
                if (fullResults.Count() > 3)
                {
                    workResult.AddCandidates(rankings.Take(3));
                }
                else
                {
                    workResult.AddCandidates(rankings);
                }
                for (int i = 0; i < workResult.Results.Count; i++)
                {
                    workResult.IndividualScores[i] = 0.45 + 0.6 * (1.0 * param.Length / workResult.Results[i].Length);
                    string[] split = TextProcessing.Tokenize(param);
                    for (int j = 0; j < split.Length; j++)
                    {
                        workResult.IndividualScores[i] *= (1.0 * StaticDictionaries.UnigramDictionary.Count - StaticDictionaries.GetUnigramFrequency(split[j])) / StaticDictionaries.UnigramDictionary.Count;
                    }
                }
            }
            //if (!TaskObsolete)
            //{
            //    client.CancelPendingRequests();
            //}
#if PERF
            Tock();
#endif
            return workResult;
        }
    }
}
