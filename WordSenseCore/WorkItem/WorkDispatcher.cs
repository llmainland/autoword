﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
namespace WordSense.WorkItem
{
    public class WorkCompletedEventArgs : EventArgs
    {
        public WorkResult Result { get; set; }
        public WorkCompletedEventArgs(WorkResult completedResult)
        {
            Result = completedResult;
        }
    }

    public class WorkExceptionEventArgs : EventArgs
    {
        public Workload ProblematicWorkload { get; set; }
        public WorkExceptionEventArgs(Workload faultyWorkLoad)
        {
            ProblematicWorkload = faultyWorkLoad;
        }
    }

    public class WorkDispatcher
    {
        public WorkDispatcher()
        {
            pendingTasks = new List<Task<WorkResult>>();
            resultWorkloadPair = new Dictionary<Task<WorkResult>, Workload>();
            ticket = Workload.RequestNewId();
        }

        /*public void FetchNewTicket()
        {
            ticket = Workload.RequestNewId();
        }*/
        static UsageStatistics usageStats = new UsageStatistics(50, 0.9);
        long ticket = 0;
        IList<Task<WorkResult>> pendingTasks;
        Dictionary<Task<WorkResult>, Workload> resultWorkloadPair;
        public event EventHandler<WorkCompletedEventArgs> OnObservableTaskCompletion;
        public event EventHandler<WorkExceptionEventArgs> OnTaskFault;

        void fireTaskFaultNotification(Workload errorLoad)
        {
            if (OnTaskFault == null) return;
            if (ticket != Workload.GetGlobalRequestId()) return;
            OnTaskFault(this, new WorkExceptionEventArgs(errorLoad));
        }

        void fireTaskCompletion(WorkResult workResult)
        {
            //if (workResult.Results.Count() == 0) return;
            if (OnObservableTaskCompletion == null) return;
            if (ticket != Workload.GetGlobalRequestId()) return;
            OnObservableTaskCompletion(this, new WorkCompletedEventArgs(workResult));
        }
        Dictionary<Task<WorkResult>, DateTime> tscTable = new Dictionary<Task<WorkResult>, DateTime>();
        static Dictionary<Type, long> averageLatency = new Dictionary<Type, long>();
        static long lastResetTicket = long.MinValue + 1;
        public void QueueItems(params Workload[] items)
        {
            usageStats.keyRequest();
            foreach (var item in items)
            {
                if (averageLatency.ContainsKey(item.GetType()) == false)
                {
                    averageLatency.Add(item.GetType(), 0);
                }

                if (ticket - lastResetTicket >= 10 && averageLatency[item.GetType()] >= usageStats.WeightedLastKKeyPressInterval * 2 && item.Policy == Workload.ExecutePolicy.PerKey)
                {
                    if (ticket - lastResetTicket >= 50)
                    {
                        averageLatency[item.GetType()] = 0;//attempt again.
                        lastResetTicket = ticket;
                    }//do not execute. latency too long
                    continue;
                }
                var task = item.Execute();
                if (task != null)
                {
                    //this task should not be run. 
                    pendingTasks.Add(task);
                    resultWorkloadPair.Add(task, item);
                    tscTable.Add(task, DateTime.Now);
                }
            }
        }

        public async void RetrieveMergedResult()
        {
            // var results = new List<WorkResult>();
            int validMaximumResponseGoogle = -1, validMaximumResponseBing = -1;
            //for (int i = 0; i < 5000000; i++) i.ToString();
            //collect all the awaitables.
            //now spend a maximum of 100ms waiting for it.
            //Task<WorkResult>.WaitAll(pendingTasks.Keys.ToArray());
            //either all tasks have finished, or time elapsed. Collect what we have so far and let go.
            var finishedTasks = new List<WorkResult>();
            var ignoredTasks = pendingTasks.Where<Task<WorkResult>>(o => resultWorkloadPair[o].ResultPropagation == Workload.ResultPropagationType.Silent).ToList();
            foreach (var task in ignoredTasks)
            {
                tscTable.Remove(task);
                pendingTasks.Remove(task);
            }
            while (pendingTasks.Count > 0)
            {
                var key = await Task.WhenAny<WorkResult>(pendingTasks);
                Debug.Assert(key.IsCompleted);
                Workload associatedWorkload;
                pendingTasks.Remove(key);
                try
                {
                    associatedWorkload = key.Result.AssociatedWorkload;
                }
                catch
                {
                    fireTaskFaultNotification(resultWorkloadPair[key]);
                    tscTable.Remove(key);
                    continue;
                }
                resultWorkloadPair.Remove(key);
                var workloadType = associatedWorkload.GetType();
                var time = tscTable[key];
                tscTable.Remove(key);

#if PERF
                Debug.WriteLine(string.Format("request id:{0}, group id:{1}, query string:{4},request type:{2}, delay:{3}ms", associatedWorkload.RequestId, associatedWorkload.GroupId, associatedWorkload.GetType().Name, (associatedWorkload.tock - time).TotalMilliseconds, string.Join(" ", associatedWorkload.Parameters)));
#endif
                if (ticket != Workload.GetGlobalRequestId())
                {
                    cleanUp();
                    return;// Results; //fast cancellation.
                }
                if (associatedWorkload.ResultPropagation == Workload.ResultPropagationType.Individual)
                {
                    fireTaskCompletion(key.Result);
                    continue;
                }
                //special treatment for Google and Bing tasks
                var tempResult = key.Result;
                Debug.Assert(associatedWorkload == tempResult.AssociatedWorkload);
                if (workloadType == typeof(QueryCompletionWorkload))
                {
                    if (tempResult.AssociatedWorkload.Parameters.Length > validMaximumResponseGoogle && tempResult.Results.Count > 0)
                    {
                        validMaximumResponseGoogle = tempResult.AssociatedWorkload.Parameters.Length;
                        finishedTasks.Add(tempResult);
                        var collection = pendingTasks.Where<Task<WorkResult>>(o => resultWorkloadPair[o].GroupId == associatedWorkload.GroupId && resultWorkloadPair[o].Parameters.Length <= validMaximumResponseGoogle);
                        foreach (var task in collection.ToArray())
                        {
                            pendingTasks.Remove(task);
                        }
                    }
                }
                else if (workloadType == typeof(BingBPoleWorkload))
                {
                    if (tempResult.AssociatedWorkload.Parameters.Length > validMaximumResponseBing && tempResult.Results.Count > 0)
                    {
                        validMaximumResponseBing = tempResult.AssociatedWorkload.Parameters.Length;
                        finishedTasks.Add(tempResult);
                        var collection = pendingTasks.Where<Task<WorkResult>>(o => resultWorkloadPair[o].GroupId == associatedWorkload.GroupId && resultWorkloadPair[o].Parameters.Length <= validMaximumResponseBing);
                        foreach (var task in collection.ToArray())
                        {
                            pendingTasks.Remove(task);
                        }
                    }
                }
                else
                {
                    finishedTasks.Add(tempResult);
                }

                //no race. single threaded.
                if (ticket == lastResetTicket) continue;
                averageLatency[workloadType] *= (ticket - 1 - lastResetTicket);
                averageLatency[workloadType] += (long)(associatedWorkload.tock - time).TotalMilliseconds;
                averageLatency[workloadType] /= (ticket - lastResetTicket);
                //can be merged earlier?
                if (associatedWorkload.ResultPropagation == Workload.ResultPropagationType.Group)
                    if (pendingTasks.Count<Task<WorkResult>>(o => resultWorkloadPair[o].GroupId == associatedWorkload.GroupId) == 0)
                    {
                        //can be merged earlier without waiting for all.
                        var group = finishedTasks.Where<WorkResult>(o => o.AssociatedWorkload.GroupId == associatedWorkload.GroupId);
                        var finishedTask = group.Where<WorkResult>(o => o.Results.Count > 0).OrderByDescending<WorkResult, int>(o => o.AssociatedWorkload.Parameters.Length).FirstOrDefault();
                        if (finishedTask == null)
                        {
                            finishedTask = new WorkResult(-1, associatedWorkload);
                        }
                        fireTaskCompletion(finishedTask);
                    }
            }
            cleanUp();
            //return results;
        }

        private void cleanUp()
        {
            pendingTasks.Clear();
            resultWorkloadPair.Clear();
        }
    }
}
