﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using WordSense.Dictionary;
using WordSense.Trie;

namespace WordSense.WorkItem
{
    public class TextAnalyticsWorkload : Workload
    {
        static HttpClient client = new HttpClient();
        static string NERUri = "https://api.datamarket.azure.com/data.ashx/amla/text-analytics/v1/GetKeyPhrases?text=";
        static string AlchemyUrl = "http://access.alchemyapi.com/calls/text/TextGetRankedNamedEntities";
        static string AlchemyKey = "d24a51743addbf5ddb8ed260e2b929638ac85344";
        static string PrimaryKey = "F14ANwpQifGxwL36YmC8jWUB/Hy+j+YxHuZF2foKbJI";
        static volatile bool InProgress = false;
        static TextAnalyticsWorkload()
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(string.Format("{0}:{1}", "WordSense", PrimaryKey))));

        }

        public TextAnalyticsWorkload(long groupId, long editingStart, string[] parameters)
            : base(groupId, editingStart, parameters)
        {
            Policy = ExecutePolicy.PerParagraph;
            ResultPropagation = ResultPropagationType.Silent;
            Prerequisites = LaunchPrerequisites.NetworkAvaiable;
        }

        public override Task<WorkResult> Execute()
        {
            if (InProgress) return null; //do not re-enter.
            var task = Task.Run<WorkResult>(async () =>
                {
                    InProgress = true;
                    StaticDictionaries.CurrentWordFrequency.Clear();
                    StaticDictionaries.CurrentWordFrequency.AddRange(TextParsing.TextProcessing.Tokenize(Parameters[0]).GroupBy<string, string>(key => key.ToLowerInvariant()).Select<IGrouping<string, string>, KeyValuePair<string, int>>(o => new KeyValuePair<string, int>(o.Key, o.Count())));
                    var payload = string.Format("%27{0}%27", Parameters[0]);
                    var NERResponseTask = client.GetStringAsync(string.Format("{0}{1}", NERUri, payload));
                    var AlchemyResponseTaskParams = new StringBuilder();
                    AlchemyResponseTaskParams.Append("apikey=").Append(AlchemyKey).Append("&text=").Append(Parameters[0]).Append("&outputMode=json");
                    byte[] bd = UTF8Encoding.UTF8.GetBytes(AlchemyResponseTaskParams.ToString());
                    HttpWebRequest wreq = WebRequest.Create(AlchemyUrl) as HttpWebRequest;
                    wreq.Proxy = null;
                    wreq.Method = "POST";
                    wreq.ContentType = "application/x-www-form-urlencoded";
                    using (var stream = await wreq.GetRequestStreamAsync())
                    {
                        stream.Write(bd, 0, bd.Length);
                    }
                    var alchemyResponseTask = wreq.GetResponseAsync();
                    JsonObject jObj;
                    if (JsonObject.TryParse(await NERResponseTask, out jObj))
                    {
                        var jArray = jObj.GetNamedArray("KeyPhrases");
                        //StaticDictionaries.KeywordDictionary.Clear();//misspelled words should be purged.
                        if (jArray != null)
                        {
                            var candidates = jArray.Select<IJsonValue, string>(o => o.GetString()).Where<string>(o => !string.IsNullOrEmpty(o));
                            foreach (var candidate in candidates)
                            {
                                if (StaticDictionaries.KeywordDictionary.ContainsKey(candidate)) continue;
                                StaticDictionaries.KeywordDictionary.Add(new TrieEntry<int>(candidate, 0));
                            }
                        }
                    }
                    var response = await alchemyResponseTask.AsAsyncOperation<WebResponse>();
                    string alchemyResponse = "";
                    using (var stream = response.GetResponseStream())
                    {
                        var streamReader = new StreamReader(stream);
                        alchemyResponse = await streamReader.ReadToEndAsync();
                    }
                    if (JsonObject.TryParse(alchemyResponse, out jObj))
                    {
                        if (jObj.GetNamedString("status") == "OK")
                        {
                            var results = await EntityDescription.BuildFromJResult(jObj, StaticDictionaries.EntityDictionary.Keys);
                            foreach (var entityDescription in results)
                            {
                                StaticDictionaries.EntityDictionary[entityDescription.Text] = entityDescription;
                                //StaticDictionaries.KeywordDictionary[entityDescription.Text] = 0;
                            }
                        }
                    }
                    //get all okay stuff.
                    InProgress = false;
                    return null;
                });
            return task;
        }
    }
}
