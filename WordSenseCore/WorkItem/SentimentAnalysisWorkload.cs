﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using System.Linq;
using WordSense.Scoring;
using System.Diagnostics;

namespace WordSense.WorkItem
{
    public class SentimentAnalysisWorkload : Workload
    {
        static HttpClient client = new HttpClient();
        static string SENUri = "https://api.datamarket.azure.com/data.ashx/amla/text-analytics/v1/GetSentiment?text=";
        static string PrimaryKey = "F14ANwpQifGxwL36YmC8jWUB/Hy+j+YxHuZF2foKbJI";
        static SentimentAnalysisWorkload()
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(string.Format("{0}:{1}", "WordSense", PrimaryKey))));
        }

        public SentimentAnalysisWorkload(long groupId, long editingStart, string[] parameters)
            : base(groupId, editingStart, parameters)
        {
            Policy = ExecutePolicy.PerSentence;
            ResultPropagation = ResultPropagationType.Silent;
            Prerequisites = LaunchPrerequisites.None;
        }

        public override async Task<WorkResult> Execute()
        {
            Debug.WriteLine("A SENTIMENT REQUEST HAS BEEN SENT.");
            var result = new WorkResult(editingStartPosition, this);
            var payload = string.Format("%27{0}%27", Parameters[0]);
            var SENResponse = await client.GetStringAsync(string.Format("{0}{1}", SENUri, payload));

            JsonObject jObj;
            if (JsonObject.TryParse(SENResponse, out jObj))
            {
                PostResultScorer.Instance.PerfectValues[ScoringCriterion.Coherence] = 2 * (jObj.GetNamedNumber("Score") - 0.5);
            }
            Debug.WriteLine("A SENTIMENT REQUEST HAS BEEN RECEIVED.");

#if PERF
            Tock();
#endif
            return null;
        }
    }
}
