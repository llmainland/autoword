﻿using WordSense.Dictionary;
using WordSense.Trie;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows;

namespace WordSense.WorkItem
{
    public class TrieWorkload : Workload
    {
        public TrieWorkload(long requestId, long editingStart, string[] parameters, string context) : base(requestId, editingStart, parameters, context) { ResultPropagation = ResultPropagationType.Individual; }



        public override Task<WorkResult> Execute()
        {
            var task = Task<WorkResult>.Run(() =>
            {
                var result = new WorkResult(editingStartPosition, this);
                if (String.IsNullOrEmpty(Parameters[0])) return result;

                var array = StaticDictionaries.UnigramDictionary.GetByPrefix(Parameters[0]);
                array = array.Where<TrieEntry<int>>(o => o.Key.Length >= 5).OrderBy<TrieEntry<int>, int>(o => o.Value);
                var list = array.Select<TrieEntry<int>, string>(o => o.Key).ToList();
                list.Remove(Parameters[0]);
                result.AddCandidates(list.Take(2));

                //result.AddCandidates(array.Take(2).Select<TrieEntry<int>, string>(o => o.Key));
                //WorkResult.Add(result);

                // 0.65 - 0.8
                if (list.Count() == 1)
                {
                    var fullLen = result.Results[0].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[0] = 0.85 + 0.05 * fullLen / 12 + 0.03 * (StaticDictionaries.UnigramDictionary.Count - wordFreq(result.Results[0])) / StaticDictionaries.UnigramDictionary.Count;
                }
                else if (list.Count() > 1)
                {
                    var fullLen = result.Results[0].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[0] = 0.75 + 0.05 * fullLen / 12 + 0.03 * (StaticDictionaries.UnigramDictionary.Count - wordFreq(result.Results[0])) / StaticDictionaries.UnigramDictionary.Count;

                    fullLen = result.Results[1].Length;
                    if (fullLen > 12) fullLen = 12;
                    result.IndividualScores[1] = 0.60 + 0.05 * fullLen / 12 + 0.03 * (StaticDictionaries.UnigramDictionary.Count - wordFreq(result.Results[1])) / StaticDictionaries.UnigramDictionary.Count;
                }

#if PERF
                Tock();
#endif
                return result;
            });
            return task;
        }

        private int wordFreq(string str)
        {
            int value;
            bool x = StaticDictionaries.UnigramDictionary.TryGetValue(str, out value);
            if (x)
            {
                return value;
            }
            else
            {
                return StaticDictionaries.UnigramDictionary.Count;
            }
        }
    }
}
