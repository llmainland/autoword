﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordSense.WorkItem
{
    public class WorkResult
    {
        //stub
        public void AddCandidates(IEnumerable<string> candidates, double initialConfidence = 1)
        {
            foreach (var item in candidates)
            {
                AddCandidate(item,initialConfidence);
            }
        }

        public void AddCandidate(string candidate, double initialConfidence = 1)
        {
            if (string.IsNullOrEmpty(candidate.Trim())) return;
            Candidates.Add(candidate);
            IndividualScores.Add(initialConfidence);
        }

        public IList<double> IndividualScores { get; set; }
        public IList<string> Results { get { return Candidates; } }
        List<string> Candidates { get; set; }
        public long EditingStartOffset { get; private set; }
        public Workload AssociatedWorkload { get; private set; }

        public WorkResult(long editingStartOffset, Workload workload)
        {
            AssociatedWorkload = workload;
            EditingStartOffset = editingStartOffset;
            Candidates = new List<string>();
            IndividualScores = new List<double>();
        }
    }
}
