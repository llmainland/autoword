﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Windows.Data.Json;
using WordSense.Trie;
using WordSense.Dictionary;
using System.Threading.Tasks;

namespace WordSense.WorkItem
{
    public class SynonymsWorkload : Workload
    {
        static string EntityQueryUrl = "https://api.datamarket.azure.com/Bing/Synonyms/v1/GetSynonyms?Query=";
        static string PrimaryKey = "kvCbjVvBuh7NmzdsLt04aY/kw8G49TYyxAVwJkaaEzQ="; //okay to be plain-text. A spending limit is imposed on this key so I'm not going to be broke.
        static string BasicQueryUrl = "http://words.bighugelabs.com/api/2/{0}/{1}/json";
        static string SecondaryKey = "65fe86352756baf3d975adbc2253889b";
        static HttpClient client = new HttpClient();
        static SynonymsWorkload()
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(string.Format("{0}:{1}", "WordSense", PrimaryKey))));
        }
        public override async System.Threading.Tasks.Task<WorkResult> Execute()
        {
            //expand synoymous only when the current word is not recognized in the dictionary.
            WorkResult result = new WorkResult(editingStartPosition, this);
            if (Parameters[0].Length <= 2) return result;
            var inDictionary = StaticDictionaries.UnigramDictionary.GetByPrefix(Parameters[0].ToLowerInvariant()).Count() != 0;
            var allCapitalized = TextParsing.TextProcessing.AllCapitalized(Parameters[0]);
            Task<string> bingSynonymsTask = null;
            Task<string> traditionalSynonymsTask = null;
            if (allCapitalized && !inDictionary)
            {
                var queryUrl = (string.Format("{0}%27{1}%27", EntityQueryUrl, Parameters[0]));
                bingSynonymsTask = client.GetStringAsync(queryUrl);
            }
            if (inDictionary && StaticDictionaries.WordTypeDictionary.ContainsKey(Parameters[0]) && (StaticDictionaries.WordTypeDictionary[Parameters[0]] & Scoring.WordType.Adjective) != Scoring.WordType.Unknown)
            {
                //found an adjective. Ask for related words.
                var queryUrl = string.Format(BasicQueryUrl, SecondaryKey, Parameters[0]);
                traditionalSynonymsTask = client.GetStringAsync(queryUrl);
            }
            if (bingSynonymsTask != null)
            {
                //launch Bing synonyms.
                var synonymsResult = await bingSynonymsTask.AsAsyncOperation<string>();//wait now.
                JsonObject jObj;
                if (JsonObject.TryParse(synonymsResult, out jObj))
                {
                    JsonObject dObj;
                    if ((dObj = jObj.GetNamedObject("d", null)) != null)
                    {
                        //dObj
                        JsonArray jResult;
                        if ((jResult = dObj.GetNamedArray("results", null)) != null)
                        {
                            //extract synonymous.
                            var candidates = jResult.Select<IJsonValue, string>(o => o.GetObject().GetNamedValue("Synonym").GetString()).Where<string>(o => !string.IsNullOrEmpty(o));
                            //if (candidates.Count() > 3) candidates = candidates.Take(3);//take at most 3.
                            result.AddCandidates(candidates);
                            //WorkResult.Add(result);
                        }
                    }
                }
            }
            if (traditionalSynonymsTask != null)
            {
                //almost simutaneous query.
                var traditionalSynonymsResult = string.Empty;
                traditionalSynonymsResult = await traditionalSynonymsTask.AsAsyncOperation<string>();
                JsonObject jObj;
                if (JsonObject.TryParse(traditionalSynonymsResult, out jObj))
                {
                    //traditionalSynonymsResult
                    if (jObj.ContainsKey("adjective"))
                    {
                        IEnumerable<IJsonValue> relatedArray = new JsonArray();
                        IEnumerable<IJsonValue> synArray = new JsonArray();
                        if (jObj.GetNamedObject("adjective").ContainsKey("rel"))
                            relatedArray = jObj.GetNamedObject("adjective").GetNamedArray("rel");
                        if (jObj.GetNamedObject("adjective").ContainsKey("syn"))
                            synArray = jObj.GetNamedObject("adjective").GetNamedArray("syn");
                        result.AddCandidates((from item in relatedArray.Union(synArray) select item.GetString()).Distinct().Take(3), 0.0);
                    }
                }
            }

#if PERF
            Tock();
#endif
            return result;
        }

        public SynonymsWorkload(long groupId, long startPosition, string[] parameters) : base(groupId, startPosition, parameters) 
        {
            Policy = ExecutePolicy.PerWord;
            Prerequisites = LaunchPrerequisites.NetworkAvaiable;
        }

    }
}
