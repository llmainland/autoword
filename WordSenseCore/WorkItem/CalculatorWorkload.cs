﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WordSense.WorkItem
{
    public class CalculatorWorkload : Workload
    {
        public CalculatorWorkload(long requestId, long editingStart, string[] parameters)
            : base(requestId, editingStart, parameters)
        {
        }

        public override Task<WorkResult> Execute()
        {
            var task = Task<WorkResult>.Run(() =>
            {
                WorkResult result = new WorkResult(editingStartPosition, this);
                if (TaskObsolete) return result;
                double evalResult;
                if (MathParser.MathEvaluation.TryEval(Parameters[0], out evalResult))
                {
                    result.AddCandidate(evalResult.ToString());
                }
#if PERF
                Tock();
#endif
                return result;
            });
            //task.Start();
            return task;
        }
    }
}
