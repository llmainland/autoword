﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using WordSense.Scoring;

namespace WordSense.WorkItem
{
    public class BingBPoleWorkload : Workload
    {
        static string queryUrl = "http://www.bing.com/search?q=";
        static HttpClient client = new HttpClient();
        static GrammarScorer grammarGuard = new GrammarScorer();
        static BingBPoleWorkload()
        {
            //TODO: Not any faster.
            // mock an IPhone 4 - 
            // client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293 Safari/6531.22.7");
        }
        static string createUri(string[] parameter)
        {
            return string.Format("{0}{1}", queryUrl, string.Join(TextParsing.Constants.Space.ToString(), parameter));
        }
        public BingBPoleWorkload(long requestId, long editingStart, string[] parameters)
            : base(requestId, editingStart, parameters)
        {
            Policy = ExecutePolicy.PerSentence;
            ResultPropagation = ResultPropagationType.Group;
            Prerequisites = LaunchPrerequisites.NetworkAvaiable;
        }

        async Task<WorkResult> runBingQuery()
        {
            var workResult = new WorkResult(editingStartPosition, this);

            if (TaskObsolete) return workResult;
            string response = null;
            //var score = grammarGuard.GetIndividualScore(string.Join(TextParsing.Constants.SpaceString, Parameters), "");
            //if (score >= 1.0 * (Parameters.Length - 1) / Parameters.Length)
            {
                var query = createUri(Parameters);
                response = await (await client.GetAsync(createUri(Parameters))).Content.ReadAsStringAsync();

                //for (int i = 0; i < 100000000; i++) i.ToString();
                //getAsyncTask.RunSynchronously();
                //var getContentTask = getAsyncTask.Result.Content.ReadAsStringAsync();
                //getContentTask.RunSynchronously();
                //result = getContentTask.Result;
                //*[@id="b_pole"]
                //*[@id="b_pole"]/div/div/div[1]/div
                if (TaskObsolete) return workResult;
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(response);
                //HtmlNode node = doc.GetElementbyId("b_pole");
                //if (node != null)
                //{
                //    try
                //    {
                //        if (TaskObsolete) return workResult;
                //        string name = node.FirstChild.LastChild.FirstChild.InnerText;
                //        if (!TaskObsolete && !string.IsNullOrEmpty(name))
                //        {
                //            workResult.AddCandidate(name);
                //        }
                //    }
                //    catch (NullReferenceException) { }
                //}
                //if (TaskObsolete) return workResult;
                HtmlNode translator = doc.GetElementbyId("tta_tv");
                if (translator != null)
                {
                        workResult.AddCandidate(translator.InnerText);
#if PERF
                Tock();
#endif
                    return workResult;
                }
                HtmlNode bPole = doc.GetElementbyId("b_pole");
                ////*[@id="tta_tv
                if (bPole != null)
                {
                    if (bPole.FirstChild != null && bPole.FirstChild.ChildNodes.Count > 1 && bPole.FirstChild.ChildNodes[1].FirstChild != null)
                    {
                            workResult.AddCandidate(bPole.FirstChild.ChildNodes[1].FirstChild.InnerText);
#if PERF
                    Tock();
#endif
                        return workResult;
                    }
                }
                HtmlNode ansNode = doc.GetElementbyId("b_results");
                if (ansNode != null)
                {
                    //if(ansNode.FirstChild == null) return workResult;
                    var panel = ansNode.FirstChild;
                    if (TaskObsolete || panel == null) return workResult;
                    if (panel.Attributes["class"].Value.Contains("b_ans b_top"))
                    {
                        if (panel.FirstChild != null && panel.FirstChild.ChildNodes.Count > 1)
                        {
                            workResult.AddCandidate(panel.FirstChild.ChildNodes[1].InnerText);
                        }
                        else if (panel.ChildNodes.Count > 1)
                        {
                            var gridNode = panel.ChildNodes[1];
                            if (gridNode.Attributes["class"].Value.Contains("b_gridList"))
                            {
                                //contains multiple results.
                                workResult.AddCandidates(gridNode.ChildNodes.Where<HtmlNode>(o => o.NodeType == HtmlNodeType.Element && o.Name == "ul").Select<HtmlNode, string>(o => o.InnerText));
                            }
                            else if (panel.ChildNodes[1].FirstChild != null)
                            {
                                workResult.AddCandidate(panel.ChildNodes[1].FirstChild.InnerText);
                            }
                        }
                    }
                }
            }
            // return workResult;
            //if (!TaskObsolete)
            //{
            //    client.CancelPendingRequests();
            //}

#if PERF
            Tock();
#endif
            return workResult;

        }

        public override Task<WorkResult> Execute()
        {
            Func<Task<WorkResult>> handler = new Func<Task<WorkResult>>(runBingQuery);
            Task<WorkResult> task = Task<WorkResult>.Run(handler);
            //task.Start();
            //return await runBingQuery();
            return task;
        }
    }
}
