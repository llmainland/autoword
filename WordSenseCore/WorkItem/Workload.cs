﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace WordSense.WorkItem
{
    public class WorkloadAttribute : Attribute { }

    public abstract class Workload
    {
        public enum ExecutePolicy
        {
            PerKey,
            PerWord,
            PerSentence,
            PerParagraph,
            Once,
            Periodic
        }

        public enum ResultPropagationType
        {
            /// <summary>
            /// Result is propagated once the result is received
            /// </summary>
            Individual,
            /// <summary>
            /// Result is propagated only after the group has fiished execution, aka result is merged with other workload in the same group
            /// </summary>
            Group,
            Silent
        }

        public enum LaunchPrerequisites
        {
            None,
            NetworkAvaiable
        }

        public ResultPropagationType ResultPropagation { get; internal set; }
        public ExecutePolicy Policy { get; internal set; }
        public LaunchPrerequisites Prerequisites { get; internal set; }
        //request Id works by 
        public static long GetCurrentId()
        {
            return GlobalRequestId;
        }
        public static long RequestNewId()
        {
            return ++GlobalRequestId;
        }
        static long GlobalRequestId = long.MinValue;
#pragma warning disable 1998
        public virtual async Task<WorkResult> Execute()
        {
            return null;
        }
#pragma warning restore 1998
        public long RequestId { get; private set; }
        public string Context { get; private set; }

        internal bool TaskObsolete { get { return RequestId < GlobalRequestId; } }
        /// <summary>
        /// Results with same requestId are merged.
        /// </summary>
        public long GroupId
        {
            get;
            private set;
        }
        internal DateTime tock;
        /// <summary>
        /// Stops the internal Time measurement and al performanc trackings enabled for this work load.
        /// </summary>
        internal void Tock()
        {
            tock = DateTime.Now;
        }
        /// <summary>
        /// Starting replacing cursor position
        /// </summary>
        internal long editingStartPosition;

        public Workload(long groupId, long editingStart, string[] parameters)
            : this(groupId, editingStart, parameters, string.Empty)
        {
            //Interlocked.Increment(ref GlobalRequestId);
        }

        public Workload(long groupId, long editingStart, string[] parameters, string context)
        {
            //Interlocked.Increment(ref GlobalRequestId);
            RequestId = GlobalRequestId;
            GroupId = groupId;
            Parameters = parameters;
            this.editingStartPosition = editingStart;
            Policy = ExecutePolicy.PerKey;
            ResultPropagation = ResultPropagationType.Individual;
            Prerequisites = LaunchPrerequisites.None;
            Context = context;
        }

        public string[] Parameters { get; private set; }

        internal static long GetGlobalRequestId()
        {
            return GlobalRequestId;
        }
    }


}
