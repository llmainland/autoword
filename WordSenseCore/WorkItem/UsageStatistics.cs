﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WordSense.WorkItem
{
    class UsageStatistics
    {
        private int numOfPress;
        private double factorOnNew;
        private Queue<DateTime> timeQueue;
        private double oldInterval;
        private double newInterval;
        public UsageStatistics(int numOfPress, double factorOnNew)
        {
            this.numOfPress = numOfPress;
            this.factorOnNew = factorOnNew;
            this.oldInterval = 0;
            this.newInterval = 0;
            timeQueue = new Queue<DateTime>(numOfPress);
        }
        public double LastKKeyPressInterval
        {
            get
            {
                return this.newInterval;
            }
        }
        public double WeightedLastKKeyPressInterval
        {
            get
            {
                return ((1 - this.factorOnNew) * this.oldInterval + this.factorOnNew * this.newInterval);
            }
        }
        public void keyRequest()
        {
            this.oldInterval = this.newInterval;
            if (timeQueue.Count() >= this.numOfPress)
            {
                timeQueue.Dequeue();
                timeQueue.Enqueue(DateTime.Now);
            }
            else
            {
                timeQueue.Enqueue(DateTime.Now);
            }
            this.newInterval = (timeQueue.Last() - timeQueue.Peek()).TotalMilliseconds / timeQueue.Count();
            return;
        }
    }
}
