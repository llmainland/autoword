﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Data.Xml.Dom;
using System.Linq;

namespace WordSense.Dictionary
{
    public enum EntityCategory
    {
        Company,
        Person,
        Other
    }

    [Flags]
    public enum EntitySubcategory : long
    {
        None = 0,
        Brand = 1,
        MusicalArtist = 2,
        AcademicScholar = 4,
        OperatingSystemDeveloper = 8,
        ProcessorManufacturer = 16,
        SoftwareDeveloper = 32,
        VentureFundedCompany = 64,
        VideoGameDeveloper = 128,
        VideoGamePublisher = 256,
        ProgrammingLanguageDesigner = 512
    }

    public struct EntityDescription
    {
        public string Text { get; set; }
        public string DbLink { get; set; }
        public EntityCategory Category { get; set; }
        public EntitySubcategory SubCategory { get; set; }
        public IEnumerable<string> RelatedContent { get; set; }

        public static async Task<IEnumerable<EntityDescription>> BuildFromJResult(JsonObject obj, IEnumerable<string> exclusions)
        {
            var client = new HttpClient();
            var entities = obj.GetNamedArray("entities");
            var lst = new List<EntityDescription>();
            foreach (var value in entities)
            {
                var currentEntity = new EntityDescription();
                currentEntity.Text = value.GetObject().GetNamedString("text");
                if (exclusions.Contains<string>(currentEntity.Text)) continue;
                currentEntity.SubCategory = EntitySubcategory.None;
                EntityCategory category;
                StaticDictionaries.KeywordDictionary[currentEntity.Text] = 0;
                if (Enum.TryParse<EntityCategory>(value.GetObject().GetNamedString("type"), out category))
                {
                    currentEntity.Category = category;
                }
                else
                {
                    continue; //category must be company or person before we can perform intelliSense.
                }
                Task<string> quoteRetrivealTask = null;
                if (currentEntity.Category == EntityCategory.Person)
                {
                    quoteRetrivealTask = client.GetStringAsync(string.Format("http://www.stands4.com/services/v2/quotes.php?uid=3938&tokenid=EmBr8r1OTaBvJqdA&searchtype=AUTHOR&query={0}", currentEntity.Text));
                }
                if (value.GetObject().ContainsKey("disambiguated") && value.GetObject().GetNamedObject("disambiguated").ContainsKey("dbpedia"))
                {
                    currentEntity.DbLink = value.GetObject().GetNamedObject("disambiguated").GetNamedString("dbpedia");
                    var dbPediaResponseTask = client.GetStringAsync(string.Format("{0}{1}", "http://lookup.dbpedia.org/api/search.asmx/KeywordSearch?QueryString=", currentEntity.Text));
                    var relatedContentXml = await dbPediaResponseTask;

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(relatedContentXml);
                    var docs = doc.ChildNodes[1].ChildNodes.Where<IXmlNode>(o =>
                        {
                            if (o.ChildNodes.Count > 1 && o.ChildNodes[1].FirstChild != null)
                            {
                                var content = o.ChildNodes[1].FirstChild.InnerText;
                                if (content.ToLowerInvariant().StartsWith(currentEntity.Text.ToLowerInvariant()))
                                {
                                    return false;
                                }
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }).Select<IXmlNode, string>(o =>string.Format(" {0}", o.ChildNodes[1].FirstChild.InnerText));
                    currentEntity.RelatedContent = docs;
                    //currentEntity.RelatedContent.Count();//force evaluate.
                }
                var quoteXml = quoteRetrivealTask == null ? string.Empty : await quoteRetrivealTask;
                if (quoteXml != string.Empty)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(quoteXml);
                    var quotes = doc.ChildNodes[1].ChildNodes.Select<IXmlNode, string>(o => string.Format(" \"{0}\"", o.FirstChild.InnerText));
                    currentEntity.RelatedContent = currentEntity.RelatedContent.Union<string>(quotes.Take(quotes.Count() > 5 ? 5 : quotes.Count()));
                }
                EntitySubcategory subCategory;

                if (obj.ContainsKey("disambiguated"))
                {
                    if (obj.GetNamedObject("disambiguated").ContainsKey("subType"))
                    {
                        var subType = obj.GetNamedArray("subType");
                        foreach (var json in subType)
                        {
                            if (Enum.TryParse<EntitySubcategory>(json.GetString(), out subCategory))
                            {
                                currentEntity.SubCategory |= subCategory;
                            }
                        }
                    }
                }
                lst.Add(currentEntity);
            }
            return lst;
        }

    }
}
