﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using WordSense.Trie;
using WordSense.Scoring;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace WordSense.Dictionary
{
    public class DictionaryInitializationReadyEventArgs : EventArgs
    {

    }


    public static class StaticDictionaries
    {
        static Trie<int> unigramDictTrie = new Trie<int>();
        static Trie<int> bigramDictTrie = new Trie<int>();
        static Trie<int> keywordDictTrie = new Trie<int>();
        static Trie<int> empty_Int_Trie = new Trie<int>();

        //static Dictionary<string, int> empty_Str_Int_Dict = new Dictionary<string, int>();
        static Dictionary<string, int> emotionalDict = new Dictionary<string, int>();
        static ConcurrentDictionary<string, EntityDescription> entityDictionary = new ConcurrentDictionary<string, EntityDescription>();
        static Dictionary<string, WordType> wordTypeDictionary = new Dictionary<string, WordType>();
        static Dictionary<string, string> wordTransformationDictionary = new Dictionary<string, string>();
        static ConcurrentDictionary<string, int> currentWordFreq = new ConcurrentDictionary<string, int>();
        static AutoResetEvent initializationSynchronizer = new AutoResetEvent(false);
        public static event EventHandler<DictionaryInitializationReadyEventArgs> InitializationReady;

        public static void InitializeAsync()
        {
            Task.Run(() => InitDictionaries());
        }

        public static void InitializeSync()
        {
            if (DictionaryReady) return;
            InitDictionaries();
            initializationSynchronizer.WaitOne();
        }

        private static int getValue(IDictionary<string,int> dictionary, string key,int defaultValue)
        {
            int value;
            bool x = dictionary.TryGetValue(key, out value);
            if (x)
            {
                return value;
            }
            else
            {
                return defaultValue;
            }
        }

        public static int GetBigramFrequency(string str)
        {
            return getValue(bigramDictTrie, str, bigramDictTrie.Count);
        }

        public static int GetUnigramFrequency(string str)
        {
            return getValue(unigramDictTrie, str, 1);
        }

        public static int GetCurrentFrequency(string str) {
            return getValue(currentWordFreq, str, currentWordFreq.Count);
        }

        static StaticDictionaries()
        {
            //Task.Run(() => InitDictionaries());
        }

        /// <summary>
        /// Determine whether the dictionaries are initialized okay. The processor is free to reorder instructions, but not pass this variable. Used for  synchronization.
        /// </summary>
        public static bool DictionaryReady
        {
            get;
            private set;
        }

        async static Task<bool> InitDictionaries()
        {
            var vocabularyPath = "count_1w_out.txt";
            //var path = System.IO.Path.Combine(@"Dictionary", vocabularyPath);

            var _Folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            _Folder = await _Folder.GetFolderAsync(@"WordSenseCore\Dictionary");
            var _File = await _Folder.GetFileAsync(vocabularyPath);
            Debug.Assert(_File != null, "Acquire File");
            var _ReadThis = await Windows.Storage.FileIO.ReadTextAsync(_File);
            string[] words = _ReadThis.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            unigramDictTrie.AddRange(words.Select(w =>
            {
                var wds = w.Split(TextParsing.Constants.Space);
                return new TrieEntry<int>(wds[1], int.Parse(wds[0]));
            }));

            vocabularyPath = "bigram.txt";
            //path = System.IO.Path.Combine(@"..\WordSense.Shared\Dictionary", vocabularyPath);
            _File = await _Folder.GetFileAsync("bigram.txt");
            Debug.Assert(_File != null, "Acquire File");
            _ReadThis = await Windows.Storage.FileIO.ReadTextAsync(_File);
            words = _ReadThis.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            bigramDictTrie.AddRange(words.Select(w =>
            {
                var wds = w.Split('#');
                return new TrieEntry<int>(wds[0], int.Parse(wds[1]));
            }));
            CombinedSingleAndBigramDictionary = new[] { unigramDictTrie, bigramDictTrie };
            CombinedDictionary = new[] { unigramDictTrie, bigramDictTrie, keywordDictTrie };

            //var wordTypePath = "wordTypeDict.txt";
            //_File = await _Folder.GetFileAsync(wordTypePath);
            //var wordTypeDictContent = await (Windows.Storage.FileIO.ReadTextAsync(_File));
            //Dictionary<string, WordType> conversionTable = new Dictionary<string, WordType>();
            //conversionTable["adj"] = WordType.Adjective;
            //conversionTable["adv"] = WordType.Adverb;
            //conversionTable["n"] = WordType.Noun;
            //conversionTable["v"] = WordType.Verb;
            //conversionTable["prep"] = WordType.Preposition;
            //conversionTable["num"] = WordType.Noun | WordType.Adjective;
            //conversionTable["int"] = WordType.Interjection;
            //conversionTable["conj"] = WordType.Conjunction;
            //conversionTable["pron"] = WordType.Pronoun;
            //conversionTable["aux"] = WordType.Unknown;
            //foreach (var line in wordTypeDictContent.Split(new[] { TextParsing.Constants.CarriageReturn, TextParsing.Constants.LineFeed }, StringSplitOptions.RemoveEmptyEntries))
            //{
            //    var segments = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            //    Debug.Assert(segments.Length == 2);
            //    var classes = segments[1].Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            //    var word = segments[0];
            //    wordTypeDictionary[word] = WordType.Unknown;
            //    foreach (var ws in classes)
            //    {
            //        wordTypeDictionary[word] |= conversionTable[ws];
            //    }
            //}

            //var transformationDictPath = "irregularRecoveryDict.txt";
            //_File = await _Folder.GetFileAsync(transformationDictPath);
            //var transformationDictContent = await Windows.Storage.FileIO.ReadTextAsync(_File);
            //foreach (var line in transformationDictContent.Split(new[] { TextParsing.Constants.CarriageReturn, TextParsing.Constants.LineFeed }, StringSplitOptions.RemoveEmptyEntries))
            //{
            //    var segments = line.Split(new[] { ',', TextParsing.Constants.Space }, StringSplitOptions.RemoveEmptyEntries);
            //    //Debug.Assert(segments.Length == 2);
            //    var word = segments[0];
            //    foreach (var segment in segments.Skip(1))
            //    {
            //        wordTransformationDictionary[word] = segment;
            //    }
            //}

            emotionalDict["loving"] = 1;
            emotionalDict["love"] = 1;
            emotionalDict["loves"] = 1;
            emotionalDict["lovely"] = 1;
            emotionalDict["loathing"] = -1;
            emotionalDict["hate"] = -1;
            emotionalDict["hates"] = -1;
            emotionalDict["crap"] = -1;
            emotionalDict["good"] = 1;
            emotionalDict["bad"] = -1;
            emotionalDict["terrible"] = -1;
            emotionalDict["sex"] = int.MinValue;
            emotionalDict["gay"] = int.MinValue;
            emotionalDict["fuck"] = int.MinValue; //damn. act as a curse filter.

            if (InitializationReady != null)
            {
                InitializationReady(null, new DictionaryInitializationReadyEventArgs());
            }
            DictionaryReady = true;
            initializationSynchronizer.Set();
            return true;
        }
        public static Trie<int> UnigramDictionary
        {
            get { return DictionaryReady ? unigramDictTrie : empty_Int_Trie; }
        }

        public static Trie<int> BigramDictionary
        {
            get { return DictionaryReady ? bigramDictTrie : empty_Int_Trie; }
        }

        public static Trie<int> KeywordDictionary
        {
            get { return DictionaryReady ? keywordDictTrie : empty_Int_Trie; }
        }

        public static ConcurrentDictionary<string, EntityDescription> EntityDictionary
        {
            get { return entityDictionary; }
        }

        public static Dictionary<string, int> EmotionalDictionary
        {
            get { return emotionalDict; }
        }

        public static Dictionary<string, WordType> WordTypeDictionary
        {
            get { return wordTypeDictionary; }
        }

        public static Dictionary<string, string> WordTransformationRecoveryDictionary
        {
            get { return wordTransformationDictionary; }
        }

        public static ConcurrentDictionary<string, int> CurrentWordFrequency
        {
            get { return currentWordFreq; }
        }

        public static IEnumerable<Trie<int>> CombinedSingleAndBigramDictionary;
        public static IEnumerable<Trie<int>> CombinedDictionary;
    }
}
